//
//  ActivityCustom.m
//  CustomActivity
//
//  Created by Amrita on 01/09/14.
//  Copyright (c) 2014 NCR Technosolutions Pvt LTD. All rights reserved.
//

#import "ActivityCustom.h"
@implementation ActivityCustom
- (NSString *)activityType
{
    return @"yourappname.Review.App";
}

- (NSString *)activityTitle
{
    if(self.mapActivity==2){
        return @"Open in Google Map";
    }else{
        return @"Open in Maps";
    }
    
}

- (UIImage *)_activityImage
{
    // Note: These images need to have a transparent background and I recommend these sizes:
    // iPadShare@2x should be 126 px, iPadShare should be 53 px, iPhoneShare@2x should be 100
    // px, and iPhoneShare should be 50 px. I found these sizes to work for what I was making.
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        return [UIImage imageNamed:@"iPadShare.png"];
    }
    else
    {
        if(self.mapActivity==2){
        return [UIImage imageNamed:@"Icon_GoogleMaps.png"];
        }else{
        return [UIImage imageNamed:@"AppleIcon_Maps.png"]; 
        }
    }
}

- (BOOL)canPerformWithActivityItems:(NSArray *)activityItems
{
    NSLog(@"%s", __FUNCTION__);
    return YES;
}

- (void)prepareWithActivityItems:(NSArray *)activityItems
{
    NSLog(@"%s",__FUNCTION__);
}

- (UIViewController *)activityViewController
{
    NSLog(@"%s",__FUNCTION__);
    return nil;
}

- (void)performActivity
{
    NSString *strGoogle = [NSString stringWithFormat:@"comgooglemaps://?center=%lf,%lf&zoom=14&views=traffic",self.latt,self.longi];
    NSString *strApple = [NSString stringWithFormat:@"http://maps.apple.com/?sspn=%lf,%lf",self.latt,self.longi];
    if(self.mapActivity ==2 ){
       if ([[UIApplication sharedApplication] canOpenURL:
             [NSURL URLWithString:@"comgooglemaps://"]]) {
            [[UIApplication sharedApplication] openURL:
             [NSURL URLWithString:strGoogle]];
        } else {
            [[UIApplication sharedApplication] openURL:
             [NSURL URLWithString:strApple]];
        }

    }else{
       
        [[UIApplication sharedApplication] openURL:
         [NSURL URLWithString:strApple]];
    }
    [self activityDidFinish:YES];
}
@end
