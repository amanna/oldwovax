//
//  CustomPostListCell4.h
//  WovaxUniversalApp
//
//  Created by Amrita on 23/05/14.
//  Copyright (c) 2014 Susim Samanta. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Post.h"


@interface CustomPostListCell4 : UITableViewCell{
    
}
- (void)setUIWithDataSourceImage:(Post *)recentpost forIndexPath:(NSIndexPath *)indexPath;
+ (CGFloat)getDynamicHeightOfCellOfPost:(Post*)post;
+ (CGFloat)getDynamicHeightOfCellOfPostIpad:(Post*)post;
- (void)loadImageWithURLString:(NSString *)urlString forIndexPath:(NSIndexPath *)indexPath;
@property (weak, nonatomic) IBOutlet UIView *myView;
@property (weak, nonatomic) IBOutlet UILabel *lblPostTitle;
@end
