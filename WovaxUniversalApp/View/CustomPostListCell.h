//
//  CustomPostListCell.h
//  WovaxUniversalApp
//
//  Created by Amrita on 03/12/13.
//  Copyright (c) 2013 Susim Samanta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Post.h"
@interface CustomPostListCell : UITableViewCell{
}
- (void)setUIWithDataSource:(Post *)recentpost;
+ (CGFloat)getDynamicHeightOfCellOfPost:(Post*)post;
+ (CGFloat)getDynamicHeightOfCellOfPostIpad:(Post*)post;
@property (weak, nonatomic) IBOutlet UIView *myView;
@property (weak, nonatomic) IBOutlet UILabel *lblPostTitle;
@end
