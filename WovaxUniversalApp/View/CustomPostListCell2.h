//
//  CustomPostListCell2.h
//  WovaxUniversalApp
//
//  Created by Amrita on 19/05/14.
//  Copyright (c) 2014 Susim Samanta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Post.h"
@interface CustomPostListCell2 : UITableViewCell
+ (CGFloat)getDynamicHeightOfCellOfPost:(Post*)post;
+ (CGFloat)getDynamicHeightOfCellOfPostIpad:(Post*)post;
- (void)setUIWithDataSource:(Post *)recentpost;
@end
