//
//  CustomPostListCell4.m
//  WovaxUniversalApp
//
//  Created by Amrita on 23/05/14.
//  Copyright (c) 2014 Susim Samanta. All rights reserved.
//

#import "CustomPostListCell4.h"
#import "UtiltiyManager.h"
#import "SDImageCache.h"
@interface CustomPostListCell4(){
    //SDWebImageManager *manager;
    NSIndexPath *currentLoadingIndexPath;
}
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPostdt;
@property (weak, nonatomic) IBOutlet UILabel *lblSeperator;
@property (weak, nonatomic) IBOutlet UIImageView *imgThumb;


@end
@implementation CustomPostListCell4
- (void)setUIWithDataSourceImage:(Post *)recentpost forIndexPath:(NSIndexPath *)indexPath {
    CGFloat widt;
    if(UI_USER_INTERFACE_IDIOM()== UIUserInterfaceIdiomPhone){
        widt = 290;
    }else{
        widt = 728;
    }
    CGFloat htView;
    if(UI_USER_INTERFACE_IDIOM()== UIUserInterfaceIdiomPhone){
        htView = [CustomPostListCell4 getDynamicHeightOfCellOfPost:recentpost];
    }else{
       htView = [CustomPostListCell4 getDynamicHeightOfCellOfPostIpad:recentpost];
    }
   // NSLog(@"viewHeight=%frecentpost=%@",htView,recentpost.postTitle);
    
    CGRect frame = self.myView.frame;
    frame.size.height = htView-5;
    self.myView.frame = frame;
    self.myView.layer.borderColor = COLOR_CUSTOM(178, 178, 178).CGColor;
    self.myView.layer.borderWidth =0.5f;
    
    CGSize sizeOfLabel;
    CGFloat yAxis=5;
    recentpost.postTitle = [UtiltiyManager decodeHTMLCharacterEntities:recentpost.postTitle];
    sizeOfLabel = [recentpost.postTitle boundingRectWithSize:CGSizeMake(widt,400) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:iPhone? NORMAL_BOLD_FONT: HEADER_FONT_IPAD} context:nil].size;
    self.lblTitle.font = iPhone? NORMAL_BOLD_FONT: HEADER_FONT_IPAD;
    self.lblTitle.frame =  CGRectMake(self.lblTitle.frame.origin.x, yAxis,widt , sizeOfLabel.height);
    self.lblTitle.text = recentpost.postTitle;
    [self.lblPostTitle sizeThatFits:CGSizeMake(sizeOfLabel.width, sizeOfLabel.height)];
    yAxis +=sizeOfLabel.height +5;
    sizeOfLabel = [recentpost.postDate boundingRectWithSize:CGSizeMake(widt,400) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:iPhone?DATE_FONT:DATE_FONT_IPAD} context:nil].size;
    

    NSString *str = [NSString stringWithFormat:@"%@ by %@",recentpost.postDate,recentpost.postBy];
    self.lblPostdt.font = iPhone?DATE_FONT:DATE_FONT_IPAD;
    self.lblPostdt.frame =  CGRectMake(self.lblPostdt.frame.origin.x, yAxis,widt , sizeOfLabel.height);
    self.lblPostdt.text = str;
 yAxis +=sizeOfLabel.height +5;
   
   self.imgThumb.frame = CGRectMake(self.imgThumb.frame.origin.x,yAxis,self.imgThumb.frame.size.width ,recentpost.fltHeight);
    
    currentLoadingIndexPath = indexPath;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^(void) {
        NSURL *imageURL = [NSURL URLWithString:recentpost.postThumb];
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        UIImage* image = [[UIImage alloc] initWithData:imageData];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
             [self.imgThumb setImage:image];
            if (currentLoadingIndexPath != indexPath)
                     {
                         return;
                     }
               
            
                     else
                     {
                         [self.imgThumb setImage:image];
                     }

            
        });
    });
//    currentLoadingIndexPath = indexPath;
//    [self.imgThumb sd_cancelCurrentImageLoad];
//    [self.imgThumb setImage:nil];
//    
//    NSURL *imageURL = [NSURL URLWithString:recentpost.postThumb];
//    [self.imgThumb sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"blank.png"] options:SDWebImageRetryFailed progress:^(NSInteger receivedSize, NSInteger expectedSize) {
//        
//    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//        if (currentLoadingIndexPath != indexPath)
//        {
//            return;
//        }
//        
//        if (error)
//        {
//            
//        }
//        else
//        {
//            [self.imgThumb setImage:image];
//        }
//
//    }];
    
   //    
//    NSData *data = [self imageDataForUrl:imageURL];
//    UIImage *image = [UIImage imageWithData:imageData];
//    dispatch_async(dispatch_get_main_queue(), ^{
//        imageView.image = image;
//    });
    
    
//    [manager downloadWithURL:[NSURL URLWithString:recentpost.postThumb]
//                     options:0
//                    progress:^(NSUInteger receivedSize, long long expectedSize)
//     
//     {
//         // progression tracking code
//         
//     }
//                   completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished)
//     {
//         if (image)
//         {
//             
//             [self.imgThumb setImage:image];
//             
//         }else{
//             
//         }
//         
//     }];

    
    //
//   if (![recentpost.postThumb isEqual:[NSNull null]]){
//       __block NSData *imageData ;
//       dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
//       dispatch_async(queue, ^{
//           imageData = [UtiltiyManager getImageDataWithFileName:recentpost.postId];
//           if (!imageData) {
//               //NSLog(@"store%@",recentpost.postId);
//               imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:recentpost.postThumb]];
//               [UtiltiyManager storeImageData:imageData withName:recentpost.postId];
//           }
//           dispatch_sync(dispatch_get_main_queue(), ^{
//               //NSLog(@"Get%@",recentpost.postId);
//               [self.imgThumb setImage:[UIImage imageWithData:imageData]];
//           });
//       });
//   }
   yAxis +=recentpost.fltHeight +5;
   sizeOfLabel = [recentpost.postExcerpt boundingRectWithSize:CGSizeMake(widt,1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:iPhone?NORMAL_FONT:NORMAL_FONT_IPAD} context:nil].size;
   self.lblPostTitle.font = iPhone?NORMAL_FONT:NORMAL_FONT_IPAD;
   self.lblPostTitle.frame = CGRectMake(self.lblPostTitle.frame.origin.x,yAxis,widt , sizeOfLabel.height);
   self.lblPostTitle.numberOfLines = 0;
   
  NSString *poString =[UtiltiyManager decodeHTMLCharacterEntities:recentpost.postExcerpt];
   
   NSString* strWithoutFormatting = [self stringByStrippingHTML:poString];
   strWithoutFormatting = [strWithoutFormatting stringByReplacingOccurrencesOfString:@"[&hellip;]"
                                                                          withString:@"[...]"];
   self.lblPostTitle.text = strWithoutFormatting;
   yAxis +=sizeOfLabel.height +5;
   
   self.lblSeperator.frame =  CGRectMake(self.lblSeperator.frame.origin.x, yAxis,widt , 5);
  
    
}
-(NSString *)stringByStrippingHTML:(NSString*)str
{
    NSRange r;
    while ((r = [str rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location     != NSNotFound)
    {
        str = [str stringByReplacingCharactersInRange:r withString:@""];
    }
    return str;
}

+ (CGFloat)getDynamicHeightOfCellOfPost:(Post*)post {
    //NSLog(@"posttitle%@",post.postTitle);
    CGFloat yAxis=5;
    CGSize sizeOfLabel;
    sizeOfLabel = [post.postTitle boundingRectWithSize:CGSizeMake(290,400) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:NORMAL_BOLD_FONT} context:nil].size;
    
    yAxis +=sizeOfLabel.height +5;
    sizeOfLabel = [post.postDate boundingRectWithSize:CGSizeMake(290,400) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:DATE_FONT} context:nil].size;
    yAxis +=sizeOfLabel.height +5;
    yAxis +=post.fltHeight +5;
    sizeOfLabel = [post.postExcerpt boundingRectWithSize:CGSizeMake(290,1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:NORMAL_FONT} context:nil].size;
    //NSLog(@"posttitle%@",post.postExcerpt);
    //NSLog(@"post%f",sizeOfLabel.height);
    yAxis +=sizeOfLabel.height +5;
    return (yAxis + 5);
    
}
+ (CGFloat)getDynamicHeightOfCellOfPostIpad:(Post*)post{
    CGFloat yAxis=5;
    CGSize sizeOfLabel;
    sizeOfLabel = [post.postTitle boundingRectWithSize:CGSizeMake(728,400) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:HEADER_FONT_IPAD} context:nil].size;
    yAxis +=sizeOfLabel.height +5;
    sizeOfLabel = [post.postDate boundingRectWithSize:CGSizeMake(728,400) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:DATE_FONT_IPAD} context:nil].size;
    yAxis +=sizeOfLabel.height +5;
    yAxis +=post.fltHeight +5;
    sizeOfLabel = [post.postExcerpt boundingRectWithSize:CGSizeMake(728,1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:NORMAL_FONT_IPAD} context:nil].size;
    yAxis +=sizeOfLabel.height +5;
    return (yAxis + 10);
    

}
@end
