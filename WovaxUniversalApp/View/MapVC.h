//
//  MapVC.h
//  WovaxUniversalApp
//
//  Created by Amrita on 25/07/14.
//  Copyright (c) 2014 Susim Samanta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Post.h"
#import <MapKit/MapKit.h>
@interface MapVC : UIViewController<MKMapViewDelegate>
@property(nonatomic,strong)Post *post;
@end
