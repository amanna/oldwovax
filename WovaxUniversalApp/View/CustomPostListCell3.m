//
//  CustomPostListCell3.m
//  WovaxUniversalApp
//
//  Created by Amrita on 19/05/14.
//  Copyright (c) 2014 Susim Samanta. All rights reserved.
//

#import "CustomPostListCell3.h"
#import "UtiltiyManager.h"
@interface CustomPostListCell3()
@property (weak, nonatomic) IBOutlet UIImageView *imgThumb;
@property (weak, nonatomic) IBOutlet UIView *myView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPostedBy;
@property (weak, nonatomic) IBOutlet UILabel *lblText;
@property (weak, nonatomic) IBOutlet UILabel *lblSeperator;


@end
@implementation CustomPostListCell3

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void)setUIWithDataSource:(Post *)recentpost {
    CGFloat widt;
    if(UI_USER_INTERFACE_IDIOM()== UIUserInterfaceIdiomPhone){
        widt = 300;
    }else{
        widt = 748;
    }
    CGFloat htView;
    if(UI_USER_INTERFACE_IDIOM()== UIUserInterfaceIdiomPhone){
        htView = [CustomPostListCell3 getDynamicHeightOfCellOfPost:recentpost];
    }else{
        htView = [CustomPostListCell3 getDynamicHeightOfCellOfPostIpad:recentpost];
    }
    
    CGRect frame = self.myView.frame;
    frame.size.height = htView - 7;
    self.myView.frame = frame;
    self.myView.layer.borderColor = COLOR_CUSTOM(163, 163, 163).CGColor;
    self.myView.layer.borderWidth = 0.5f;

    
    self.imgThumb.frame = CGRectMake(self.imgThumb.frame.origin.x,self.imgThumb.frame.origin.y,self.imgThumb.frame.size.width ,recentpost.fltHeight);
    if (![recentpost.postThumb isEqual:[NSNull null]]){
        __block NSData *imageData ;
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^{
            imageData = [UtiltiyManager getImageDataWithFileName:recentpost.postId];
            if (!imageData) {
                imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:recentpost.postThumb]];
                [UtiltiyManager storeImageData:imageData withName:recentpost.postId];
            }
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self.imgThumb setImage:[UIImage imageWithData:imageData]];
            });
        });
    }
    
    CGSize sizeOfLabel;
    CGFloat yAxis;
    yAxis = 5 + recentpost.fltHeight;
    
    recentpost.postTitle = [UtiltiyManager decodeHTMLCharacterEntities:recentpost.postTitle];
     self.lblTitle.text = recentpost.postTitle;
    sizeOfLabel = [recentpost.postTitle boundingRectWithSize:iPhone?CGSizeMake(300,400):CGSizeMake(748,400) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:iPhone? NORMAL_BOLD_FONT: HEADER_FONT_IPAD} context:nil].size;
    self.lblTitle.frame = CGRectMake(self.lblTitle.frame.origin.x,yAxis,self.lblTitle.frame.size.width ,sizeOfLabel.height);
    self.lblTitle.font = iPhone? NORMAL_BOLD_FONT: HEADER_FONT_IPAD;
     yAxis +=sizeOfLabel.height +5;
    
    sizeOfLabel = [recentpost.postDate boundingRectWithSize:iPhone?CGSizeMake(300,400):CGSizeMake(748,400) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:iPhone? DATE_FONT:DATE_FONT_IPAD} context:nil].size;
    self.lblPostedBy.frame = CGRectMake(self.lblPostedBy.frame.origin.x,yAxis,self.lblPostedBy.frame.size.width ,sizeOfLabel.height);
    self.lblPostedBy.text = recentpost.postDate;
      self.lblPostedBy.font = iPhone? DATE_FONT:DATE_FONT_IPAD;
    yAxis +=sizeOfLabel.height +10;
    sizeOfLabel = [recentpost.postExcerpt boundingRectWithSize:iPhone?CGSizeMake(300,400):CGSizeMake(748,400) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:iPhone?NORMAL_FONT:NORMAL_FONT_IPAD} context:nil].size;
    self.lblText.frame = CGRectMake(self.lblText.frame.origin.x,yAxis,self.lblText.frame.size.width ,sizeOfLabel.height);
    self.lblText.font = iPhone?NORMAL_FONT:NORMAL_FONT_IPAD;
    recentpost.postExcerpt = [UtiltiyManager decodeHTMLCharacterEntities:recentpost.postExcerpt];
    NSString* strWithoutFormatting = [self stringByStrippingHTML:recentpost.postExcerpt];
    strWithoutFormatting = [strWithoutFormatting stringByReplacingOccurrencesOfString:@"[&hellip;]"
                                                                           withString:@"[...]"];
    

    self.lblText.text = strWithoutFormatting;
    yAxis += sizeOfLabel.height + 5;
    self.lblSeperator.frame =  CGRectMake(self.lblSeperator.frame.origin.x, yAxis,widt , 5);
   
}
-(NSString *)stringByStrippingHTML:(NSString*)str
{
    NSRange r;
    while ((r = [str rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location     != NSNotFound)
    {
        str = [str stringByReplacingCharactersInRange:r withString:@""];
    }
    return str;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
+ (CGFloat)getDynamicHeightOfCellOfPostIpad:(Post*)post{
    CGFloat yAxis= post.fltHeight + 5;
    CGSize sizeOfLabel;
    UIFont *font = [UIFont systemFontOfSize:17.0];
    sizeOfLabel = [post.postTitle boundingRectWithSize:CGSizeMake(748,400) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:HEADER_FONT_IPAD} context:nil].size;;
    yAxis +=sizeOfLabel.height +5;
    sizeOfLabel = [post.postDate boundingRectWithSize:CGSizeMake(748,400) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:DATE_FONT_IPAD} context:nil].size;;
    yAxis +=sizeOfLabel.height +10;
    sizeOfLabel = [post.postExcerpt boundingRectWithSize:CGSizeMake(748,1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:NORMAL_FONT_IPAD} context:nil].size;;
    yAxis +=sizeOfLabel.height +5;
    return (yAxis+5);

}
+ (CGFloat)getDynamicHeightOfCellOfPost:(Post*)post
{
     CGFloat yAxis= post.fltHeight + 5;
    CGSize sizeOfLabel;
    sizeOfLabel = [post.postTitle boundingRectWithSize:CGSizeMake(300,400) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:NORMAL_BOLD_FONT} context:nil].size;
    yAxis +=sizeOfLabel.height +5;
    sizeOfLabel = [post.postDate boundingRectWithSize:CGSizeMake(300,400) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:DATE_FONT} context:nil].size;
    yAxis +=sizeOfLabel.height +10;
    sizeOfLabel = [post.postExcerpt boundingRectWithSize:CGSizeMake(300,1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:NORMAL_FONT} context:nil].size;
    yAxis +=sizeOfLabel.height +5;
    return (yAxis+5);
}
@end
