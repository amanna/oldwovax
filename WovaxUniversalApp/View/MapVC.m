//
//  MapVC.m
//  WovaxUniversalApp
//
//  Created by Amrita on 25/07/14.
//  Copyright (c) 2014 Susim Samanta. All rights reserved.
//

#import "MapVC.h"
#import "myAnnotation.h"
#import "ActivityCustom.h"
@interface MapVC ()
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@end

@implementation MapVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIBarButtonItem *actionBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(actionBtnClicked)];
    self.navigationItem.rightBarButtonItem = actionBtn;
   [self.mapView setMapType:MKMapTypeStandard];
   [self.mapView setZoomEnabled:YES];
   [self.mapView setScrollEnabled:YES];

   MKCoordinateRegion region = { {0.0, 0.0 }, { 0.0, 0.0 } };

   region.center.latitude = [self.post.postLatt doubleValue]; //40.105085;
   region.center.longitude = [self.post.postLongitude doubleValue]; //-83.005237;

   region.span.longitudeDelta = 0.01f;
   region.span.latitudeDelta = 0.01f;
    
   [self.mapView setRegion:region animated:YES];
    CLLocationCoordinate2D  mapCoordinate = CLLocationCoordinate2DMake(region.center.latitude ,region.center.longitude);
    NSString *subTitle = [NSString stringWithFormat:@"lon %@,latt %@",self.post.postLongitude,self.post.postLatt];
    myAnnotation *annotation = [[myAnnotation alloc] initWithCoordinate:mapCoordinate title:self.post.postTitle subtitle:subTitle];
    [self.mapView addAnnotation:annotation];
    // Do any additional setup after loading the view.
}
-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:
(id <MKAnnotation>)annotation {

    if([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    else{
    static NSString *identifier = @"myAnnotation";
    MKPinAnnotationView * annotationView = (MKPinAnnotationView*)[self.mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
    if (!annotationView)
    {
       
        annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
        annotationView.pinColor = MKPinAnnotationColorPurple;
        annotationView.animatesDrop = YES;
       UIButton  *helpButton= [UIButton buttonWithType:UIButtonTypeInfoLight];
        helpButton.tintColor = [UIColor blueColor];
        annotationView.rightCalloutAccessoryView = helpButton;
         annotationView.canShowCallout = YES;
    }else {
        annotationView.annotation = annotation;
    }
    
    return annotationView;
    }
}
- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view
calloutAccessoryControlTapped:(UIControl *)control
{
    UIActivityViewController *activityVC;
    ActivityCustom *ca = [[ActivityCustom alloc]init];
    ca.mapActivity = 1;
    if ([[UIApplication sharedApplication] canOpenURL:
         [NSURL URLWithString:@"comgooglemaps://"]]) {
        ActivityCustom *ca1 = [[ActivityCustom alloc]init];
        ca1.mapActivity = 2;
         activityVC =
        [[UIActivityViewController alloc] initWithActivityItems:nil
                                          applicationActivities:[NSArray arrayWithObjects:ca,ca1, nil]];

    } else {
        activityVC =
        [[UIActivityViewController alloc] initWithActivityItems:nil
                                          applicationActivities:[NSArray arrayWithObjects:ca, nil]];

        
    }
    
    activityVC.completionHandler = ^(NSString *activityType, BOOL completed)
    {
        NSLog(@" activityType: %@", activityType);
        NSLog(@" completed: %i", completed);
    };
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        //        self.popoverController = [[UIPopoverController alloc] initWithContentViewController:activityVC];
        //
        //        CGRect rect = [[UIScreen mainScreen] bounds];
        //
        //        [self.popoverController
        //         presentPopoverFromRect:rect inView:self.view
        //         permittedArrowDirections:0
        //         animated:YES];
    }
    else
    {
        [self presentViewController:activityVC animated:YES completion:nil];
    }
    


}

- (void)actionBtnClicked{
    NSArray *objectsToShare;
    NSString *text = self.post.postTitle;
    NSURL *url = [NSURL URLWithString:self.post.postUrl];
    if(![self.post.postType isEqualToString:@"page"]){
        NSURL *imageUrl = [NSURL URLWithString:self.post.postThumb];
        UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:imageUrl]];
        if(image==nil){
            objectsToShare = @[text, url];
        }else{
            objectsToShare = @[text, url,image];
        }
        
    }else{
        objectsToShare = @[text, url];
    }
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypePostToFacebook
                                   ];
    
    activityVC.excludedActivityTypes = excludeActivities;
    [self presentViewController:activityVC animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
