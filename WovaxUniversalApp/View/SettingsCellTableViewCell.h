//
//  SettingsCellTableViewCell.h
//  WovaxUniversalApp
//
//  Created by Amrita on 16/07/14.
//  Copyright (c) 2014 Susim Samanta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsCellTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblName;

@property (weak, nonatomic) IBOutlet UITextField *lblText;
@end
