//
//  PostDetailsCell.h
//  WovaxUniversalApp
//
//  Created by Amrita on 26/12/13.
//  Copyright (c) 2013 Susim Samanta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Post.h"
@interface PostDetailsCell : UITableViewCell{
    
}
- (void)setUIWithDataSource:(Post *)recentpost;
+ (CGFloat)getDynamicHeightOfCellOfPost:(Post*)post;
@end
