//
//  CustomPostListCell2.m
//  WovaxUniversalApp
//
//  Created by Amrita on 19/05/14.
//  Copyright (c) 2014 Susim Samanta. All rights reserved.
//

#import "CustomPostListCell2.h"
#import "UtiltiyManager.h"
@interface CustomPostListCell2()
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPostByDate;

@end


@implementation CustomPostListCell2

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void)setUIWithDataSource:(Post *)recentpost
{
     self.imgView.frame = CGRectMake(self.imgView.frame.origin.x,self.imgView.frame.origin.y,self.imgView.frame.size.width ,recentpost.fltHeight);
   
   
    if (![recentpost.postThumb isEqual:[NSNull null]]){
        __block NSData *imageData ;
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^{
            imageData = [UtiltiyManager getImageDataWithFileName:recentpost.postId];
            if (!imageData) {
                imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:recentpost.postThumb]];
                [UtiltiyManager storeImageData:imageData withName:recentpost.postId];
            }
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self.imgView setImage:[UIImage imageWithData:imageData]];
            });
        });
    }
    recentpost.postTitle = [UtiltiyManager decodeHTMLCharacterEntities:recentpost.postTitle];
    self.lblTitle.font = iPhone? NORMAL_BOLD_FONT: HEADER_FONT_IPAD;
    self.lblTitle.text = recentpost.postTitle;
    NSString *str = [NSString stringWithFormat:@"%@ by %@",recentpost.postDate,recentpost.postBy];
    self.lblPostByDate.font = iPhone?DATE_FONT:DATE_FONT_IPAD;
    self.lblPostByDate.text = str;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
+ (CGFloat)getDynamicHeightOfCellOfPost:(Post*)post
{
    return post.fltHeight + 5;
}
+ (CGFloat)getDynamicHeightOfCellOfPostIpad:(Post*)post{
    return post.fltHeight + 5;
}
@end
