//
//  PostDetailsCell.m
//  WovaxUniversalApp
//
//  Created by Amrita on 26/12/13.
//  Copyright (c) 2013 Susim Samanta. All rights reserved.
//

#import "PostDetailsCell.h"
#import "Post.h"
@interface PostDetailsCell()
@property(weak,nonatomic) IBOutlet UILabel *lblTitle;
@property(weak,nonatomic) IBOutlet UIImageView *imgThumb;
@property(weak,nonatomic) IBOutlet UILabel *lblDate;
@property(weak,nonatomic) IBOutlet UIWebView *webContent;
@end
@implementation PostDetailsCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void)setUIWithDataSource:(Post *)recentpost {
    self.lblTitle.text = recentpost.postTitle;
    self.lblDate.text = recentpost.postDate;
    [self.webContent loadHTMLString:recentpost.postContent baseURL:nil];
    if (![recentpost.postThumb isEqual:[NSNull null]]){
        __block NSData *imageData ;
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^{
            imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:recentpost.postThumb]];
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self.imgThumb setImage:[UIImage imageWithData:imageData]];
            });
        });
    }
}
+ (CGFloat)getDynamicHeightOfCellOfPost:(Post*)post {
    CGFloat yAxis=5;
    CGSize sizeOfLabel;
    UIFont *font = [UIFont systemFontOfSize:17.0];
    sizeOfLabel = [post.postTitle boundingRectWithSize:CGSizeMake(300,400) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil].size;
    yAxis +=sizeOfLabel.height +5;
    sizeOfLabel = [post.postDate boundingRectWithSize:CGSizeMake(300,400) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil].size;;
    yAxis +=sizeOfLabel.height +5;
    if (![post.postThumb isEqual:[NSNull null]]){
        yAxis +=128 +5;
    }
    sizeOfLabel = [post.postContent boundingRectWithSize:CGSizeMake(300,1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil].size;;
    yAxis +=sizeOfLabel.height +5;
    return (yAxis+5);
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
