//
//  ActivityCustom.h
//  CustomActivity
//
//  Created by Amrita on 01/09/14.
//  Copyright (c) 2014 NCR Technosolutions Pvt LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityCustom : UIActivity
@property(nonatomic)NSInteger mapActivity;
@property(nonatomic)double longi,latt;
@end
