//
//  CustomPostListCell.m
//  WovaxUniversalApp
//
//  Created by Amrita on 03/12/13.
//  Copyright (c) 2013 Susim Samanta. All rights reserved.
//

#import "CustomPostListCell.h"
#import "WebServiceManager.h"
#import <QuartzCore/QuartzCore.h>
#import "UtiltiyManager.h"
@interface CustomPostListCell()
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPostdt;
@property (weak, nonatomic) IBOutlet UILabel *lblSeperator;


@end
@implementation CustomPostListCell
-(void)layoutSubviews
{
    [super layoutSubviews];
   
   // self.contentView.superview.clipsToBounds = NO;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}
- (void)setUIWithDataSource:(Post *)recentpost {
    CGFloat widt;
    if(iPhone){
         widt= 300;
    }else{
        widt= 748;
    }
    CGFloat htView = 0.0;
     if(iPhone){
      htView = [CustomPostListCell getDynamicHeightOfCellOfPost:recentpost];
    }else{
         htView = [CustomPostListCell getDynamicHeightOfCellOfPostIpad:recentpost];
    }
    
    CGRect frame = self.myView.frame;
    frame.size.height = htView - 7;
    self.myView.frame = frame;
    self.myView.layer.borderColor = COLOR_CUSTOM(163, 163, 163).CGColor;
    self.myView.layer.borderWidth = 0.5f;
    
    CGSize sizeOfLabel;
    CGFloat yAxis=5;
    recentpost.postTitle = [UtiltiyManager decodeHTMLCharacterEntities:recentpost.postTitle];
    sizeOfLabel = [recentpost.postTitle boundingRectWithSize:CGSizeMake(widt,400) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:iPhone? NORMAL_BOLD_FONT: HEADER_FONT_IPAD} context:nil].size;
    self.lblTitle.font = iPhone? NORMAL_BOLD_FONT: HEADER_FONT_IPAD;
    self.lblTitle.frame =  CGRectMake(self.lblTitle.frame.origin.x, yAxis,widt , sizeOfLabel.height);
    self.lblTitle.text = recentpost.postTitle;
    [self.lblPostTitle sizeThatFits:CGSizeMake(sizeOfLabel.width, sizeOfLabel.height)];
    yAxis +=sizeOfLabel.height +5;
     sizeOfLabel = [recentpost.postDate boundingRectWithSize:CGSizeMake(widt,400) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:iPhone?DATE_FONT:DATE_FONT_IPAD} context:nil].size;
    NSString *str = [NSString stringWithFormat:@"%@  by  %@",recentpost.postDate,recentpost.postBy];
    self.lblPostdt.font = iPhone?DATE_FONT:DATE_FONT_IPAD;
    self.lblPostdt.frame =  CGRectMake(self.lblPostdt.frame.origin.x, yAxis,widt , sizeOfLabel.height);
    self.lblPostdt.text = str;
    
    yAxis +=sizeOfLabel.height +10;
     sizeOfLabel = [recentpost.postExcerpt boundingRectWithSize:CGSizeMake(widt,1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:iPhone?NORMAL_FONT:NORMAL_FONT_IPAD} context:nil].size;
    self.lblPostTitle.font = iPhone?NORMAL_FONT:NORMAL_FONT_IPAD;
    self.lblPostTitle.frame = CGRectMake(self.lblPostTitle.frame.origin.x,yAxis,widt , sizeOfLabel.height);
    self.lblPostTitle.numberOfLines = 0;
    recentpost.postExcerpt = [UtiltiyManager decodeHTMLCharacterEntities:recentpost.postExcerpt];
    NSString* strWithoutFormatting = [self stringByStrippingHTML:recentpost.postExcerpt];
    self.lblPostTitle.text = strWithoutFormatting;
    yAxis +=sizeOfLabel.height +5;
    
    self.lblSeperator.frame =  CGRectMake(self.lblSeperator.frame.origin.x, yAxis,widt , 5);
    
}
-(NSString *)stringByStrippingHTML:(NSString*)str
{
    NSRange r;
    while ((r = [str rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location!= NSNotFound)
    {
        str = [str stringByReplacingCharactersInRange:r withString:@""];
    }
    return str;
}
+ (CGFloat)getDynamicHeightOfCellOfPost:(Post*)post {
    
    CGFloat yAxis=5;
    CGSize sizeOfLabel;
    sizeOfLabel = [post.postTitle boundingRectWithSize:CGSizeMake(300,400) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:NORMAL_BOLD_FONT} context:nil].size;

    yAxis +=sizeOfLabel.height +5;
    sizeOfLabel = [post.postDate boundingRectWithSize:CGSizeMake(300,400) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:DATE_FONT} context:nil].size;
    yAxis +=sizeOfLabel.height +10;
    sizeOfLabel = [post.postExcerpt boundingRectWithSize:CGSizeMake(300,1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:NORMAL_FONT} context:nil].size;
    yAxis +=sizeOfLabel.height +5;
    return (yAxis + 5);

}
+ (CGFloat)getDynamicHeightOfCellOfPostIpad:(Post*)post{
    CGFloat yAxis=5;
    CGSize sizeOfLabel;
     sizeOfLabel = [post.postTitle boundingRectWithSize:CGSizeMake(748,400) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:HEADER_FONT_IPAD} context:nil].size;
    yAxis +=sizeOfLabel.height +5;
    sizeOfLabel = [post.postDate boundingRectWithSize:CGSizeMake(748,1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:DATE_FONT_IPAD} context:nil].size;
    yAxis +=sizeOfLabel.height +10;
     sizeOfLabel = [post.postExcerpt boundingRectWithSize:CGSizeMake(748,1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:NORMAL_FONT_IPAD} context:nil].size;
    yAxis +=sizeOfLabel.height +5;
    return (yAxis + 5);

    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
