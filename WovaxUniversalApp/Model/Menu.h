//
//  Menu.h
//  WovaxUniversalApp
//
//  Created by Susim Samanta on 29/10/13.
//  Copyright (c) 2013 Susim Samanta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Menu : NSObject
@property(nonatomic) NSString *menuName;
@property(nonatomic) NSString *menuPostId;
@property(nonatomic) NSString *menuObjectId;
@property(nonatomic) NSString *menuType;
@property(nonatomic) NSString *menuUrl;
@property(nonatomic) NSString *menuCustomType;
@property(nonatomic) NSString *menuEmail;
@property(nonatomic) NSString *menuPhone;
- (id)initWithDictionary:(NSDictionary *)dict;	
@end
