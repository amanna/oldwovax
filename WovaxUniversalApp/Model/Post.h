//
//  recentpost.h
//  WovaxUniversalApp
//
//  Created by Amrita on 02/12/13.
//  Copyright (c) 2013 Susim Samanta. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Post : NSObject
@property(nonatomic)NSString *postId;
@property(nonatomic)NSString *postType;
@property(nonatomic)NSString *postTitle;
@property(nonatomic)NSString *postDate;
@property(nonatomic)NSString *postBy;
@property(nonatomic)NSString *postThumb;
@property(nonatomic)NSString *postContent;
@property(nonatomic)NSString *postCategoryId;
@property(nonatomic)NSString *postCategoryTitle;

@property(nonatomic)NSString *postExcerpt;
@property(nonatomic)NSString *postTemplate;
@property(nonatomic)NSString *commentStatus;
@property(nonatomic)NSString *postUrl;
@property (nonatomic) float fltHeight;
@property(nonatomic)NSArray *postGallery;
@property(nonatomic)NSArray *postAttachment;
@property(nonatomic)NSString *postGalleryEnable;
@property(nonatomic)NSString *postMapEnable;
@property(nonatomic)NSString *postLatt;
@property(nonatomic)NSString *postLongitude;
@property(nonatomic)NSString *postHidden;
@property(nonatomic)NSString *postWebEnable;
@property(nonatomic)NSString *postCatId;
- (id)initWithDictionary:(NSDictionary *)dict;
@end
