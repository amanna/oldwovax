//
//  menudetails.m
//  WovaxUniversalApp
//
//  Created by Amrita on 16/01/14.
//  Copyright (c) 2014 Susim Samanta. All rights reserved.
//

#import "Menudetails.h"

@implementation Menudetails
#define kpostID	          @"id"
#define kpostTitle	      @"title"
#define kpostDate	      @"date"
#define kpostAuthor	      @"author"
#define kpostByname	      @"name"
#define kpostContent	  @"content"
#define kpostThumb        @"thumbnail"
#define kpostCommentCount        @"comment_count"
#define kpostCategoryId	        @"id"
#define kpostCategoryTitle	    @"id"

- (id)initWithDictionary:(NSDictionary *)dict
{
    if (self = [super init]) {
        self.postId = [NSString stringWithFormat:@"%@",dict[kpostID]];
        self.postTitle= dict[kpostTitle];
        self.postDate = dict[kpostDate];
        self.postThumb = dict[kpostThumb];
        self.postContent = dict[kpostContent];
        self.postBy = dict[kpostAuthor][kpostByname];
    }
    return self;
}
#pragma mark NSCoding
- (void) encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:_postId forKey:@"postId"];
    [encoder encodeObject:_postTitle forKey:@"postTitle"];
    [encoder encodeObject:_postDate forKey:@"postDate"];
    [encoder encodeObject:_postContent forKey:@"postContent"];
    [encoder encodeObject:_postBy forKey:@"postBy"];
    [encoder encodeObject:_postThumb forKey:@"postThumb"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    self.postId = [decoder decodeObjectForKey:@"postId"];
    self.postTitle = [decoder decodeObjectForKey:@"postTitle"];
    self.postDate = [decoder decodeObjectForKey:@"postDate"];
    self.postContent = [decoder decodeObjectForKey:@"postContent"];
    self.postBy = [decoder decodeObjectForKey:@"postBy"];
    self.postThumb = [decoder decodeObjectForKey:@"postThumb"];
    return self;
}
@end
