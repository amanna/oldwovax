//
//  recentpost.m
//  WovaxUniversalApp
//
//  Created by Amrita on 02/12/13.
//  Copyright (c) 2013 Susim Samanta. All rights reserved.
//

#import "Post.h"

@implementation Post
#define kpostID	          @"id"
#define kpostTitle	      @"title"
#define kpostDate	      @"date"
#define kpostAuthor	      @"author"
#define kpostByname	      @"name"
#define kpostContent	  @"content"
#define kpostThumb        @"thumbnail"
#define kpostexcerpt      @"excerpt"

#define kpostCategoryId	        @"id"
#define kpostCategoryTitle	    @"id"


#define kpostThumbnail   @"thumbnail_images"
#define kFull            @"full"
#define kurl             @"url"
#define kHeight          @"height"
#define kWidth          @"width"
#define kPostType       @"type"
#define kCommentstatus  @"comment_status"
#define kCustomField    @"custom_fields"
#define kGalleryEnable  @"postGalleryEnable"
#define kMapEnable  @"postMapEnable"
#define kAttachment     @"attachments"
#define kImages         @"images"

#define kCat            @"categories"
#define kId             @"id"

- (id)initWithDictionary:(NSDictionary *)dict
{
    if (self = [super init]) {
        self.postId = [NSString stringWithFormat:@"%@",dict[kpostID]];
        self.postTitle= dict[kpostTitle];
        self.postDate = dict[kpostDate];
        self.postUrl = dict[kurl];
        self.postContent = dict[kpostContent];
        self.postBy = dict[kpostAuthor][kpostByname];
        self.postExcerpt = dict[kpostexcerpt];
        self.postType = dict[kPostType];
        self.commentStatus = dict[kCommentstatus];
        self.postGallery = dict[kCustomField][kGalleryEnable];
        self.postGalleryEnable = [self.postGallery objectAtIndex:0];
        self.postAttachment = dict[kAttachment];
        
        NSArray *arrCat = [dict valueForKey:@"categories"];
        if(arrCat.count > 0){
        NSDictionary *dictCat = [arrCat objectAtIndex:0];
        self.postCatId = [NSString stringWithFormat:@"%@",dictCat[kId]];
        }
        NSDictionary *dict1 = [dict valueForKey:@"custom_fields"];
        NSArray *arr = [dict1 valueForKey:@"wovaxAppIphoneTemplate"];
        NSArray *arrMap = [dict1 valueForKey:@"postMapEnable"];
        NSArray *arrLatt = [dict1 valueForKey:@"geo_latitude"];
        NSArray *arrLongi = [dict1 valueForKey:@"geo_longitude"];
        
        NSArray *arrPostHidden = [dict1 valueForKey:@"postHiddenEnable"];
        NSArray *arrWebenable = [dict1 valueForKey:@"postWebViewEnable"];
        self.postLatt = [arrLatt objectAtIndex:0];
        self.postLongitude = [arrLongi objectAtIndex:0];
        self.postMapEnable = [arrMap objectAtIndex:0];
        self.postTemplate = [arr objectAtIndex:0];
        self.postHidden = [arrPostHidden objectAtIndex:0];
        self.postWebEnable = [arrWebenable objectAtIndex:0];
         self.postThumb = dict[kpostThumbnail][kFull][kurl];
        if(self.postThumb != nil){
        	NSString *strHt  = [NSString stringWithFormat:@"%@",dict[kpostThumbnail][kFull][kHeight]];
            NSString *strWidth  = [NSString stringWithFormat:@"%@",dict[kpostThumbnail][kFull][kWidth]];
            CGFloat fltratio = ([strWidth floatValue]) / ([strHt floatValue]);
            if(iPhone){
                self.fltHeight = 306/ fltratio;
            }else{
                self.fltHeight = 748/ fltratio;
            }
        	
        }
        
    }
    return self;
}
#pragma mark NSCoding
- (void) encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:_postId forKey:@"postId"];
    [encoder encodeObject:_postTitle forKey:@"postTitle"];
    [encoder encodeObject:_postDate forKey:@"postDate"];
    [encoder encodeObject:_postContent forKey:@"postContent"];
    [encoder encodeObject:_postBy forKey:@"postBy"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    self.postId = [decoder decodeObjectForKey:@"postId"];
    self.postTitle = [decoder decodeObjectForKey:@"postTitle"];
    self.postDate = [decoder decodeObjectForKey:@"postDate"];
    self.postContent = [decoder decodeObjectForKey:@"postContent"];
    self.postBy = [decoder decodeObjectForKey:@"postBy"];
    return self;
}
@end
