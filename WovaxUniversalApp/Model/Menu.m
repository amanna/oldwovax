//
//  Menu.m
//  WovaxUniversalApp
//
//  Created by Susim Samanta on 29/10/13.
//  Copyright (c) 2013 Susim Samanta. All rights reserved.
//

#import "Menu.h"
#define kmenuName	@"label"
#define kmenuPostId	@"id"
#define kmenuObjectId	@"object_id"
#define kmenuType	@"object_type"
#define klink  @"url"

#define kcustomtype  @"custom_type"
#define kemail  @"email_id"
#define kphone  @"phone_id"
@interface Menu ()

@end

@implementation Menu
- (id)initWithDictionary:(NSDictionary *)dict
{
    if (self = [super init]) {
        self.menuName = dict[kmenuName];
        self.menuObjectId = dict[kmenuObjectId];
        self.menuPostId = dict[kmenuPostId];
        self.menuType = dict[kmenuType];
        self.menuUrl = dict[klink];
        
        self.menuCustomType = dict[kcustomtype];
        self.menuEmail = dict[kemail];
        self.menuPhone = dict[kphone];
        
    }
    return self;
}
#pragma mark NSCoding
- (void) encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:_menuName forKey:kmenuName];
    [encoder encodeObject:_menuObjectId forKey:kmenuObjectId];
    [encoder encodeObject:_menuPostId forKey:kmenuPostId];
    [encoder encodeObject:_menuType forKey:kmenuType];
    [encoder encodeObject:_menuCustomType forKey:kcustomtype];
    [encoder encodeObject:_menuEmail forKey:kemail];
    [encoder encodeObject:_menuPhone forKey:kphone];

}


- (id)initWithCoder:(NSCoder *)decoder {
    self.menuName = [decoder decodeObjectForKey:kmenuName];
    self.menuObjectId = [decoder decodeObjectForKey:kmenuObjectId];
    self.menuPostId = [decoder decodeObjectForKey:kmenuPostId];
    self.menuType = [decoder decodeObjectForKey:kmenuType];
    
    self.menuCustomType = [decoder decodeObjectForKey:kcustomtype];
    self.menuEmail = [decoder decodeObjectForKey:kemail];
    self.menuPhone = [decoder decodeObjectForKey:kphone];
    
    
    
    return self;
}
@end
