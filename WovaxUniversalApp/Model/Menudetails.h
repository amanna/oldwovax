//
//  menudetails.h
//  WovaxUniversalApp
//
//  Created by Amrita on 16/01/14.
//  Copyright (c) 2014 Susim Samanta. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Menudetails : NSObject
@property(nonatomic)NSString *postId;
@property(nonatomic)NSString *postTitle;
@property(nonatomic)NSString *postDate;
@property(nonatomic)NSString *postBy;
@property(nonatomic)NSString *postThumb;
@property(nonatomic)NSString *postContent;
@property(nonatomic)NSString *postCategoryId;
@property(nonatomic)NSString *postCategoryTitle;
@property(nonatomic)NSArray *postComment;
- (id)initWithDictionary:(NSDictionary *)dict;
@end
