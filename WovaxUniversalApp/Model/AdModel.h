//
//  AdModel.h
//  WovaxApp
//
//  Created by Susim Samanta on 23/07/13.
//  Copyright (c) 2013 joeharby. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef enum {
    kGoogleAdMob = 0,
    kOnlyImageBanner,
    kHTMLSnippet,
    kSmallImageBanner,
    kHTMLBanner,
    kNone
    
}ADMobType;
@interface AdModel : NSObject
@property (nonatomic) ADMobType adMobType;
@property (nonatomic) NSString *adMobImageLink;
@property (nonatomic) NSString *adMobWebViewLink;
@property (nonatomic) NSString *adMobFullImageLink;
@property (nonatomic) NSString *adMobFullImageIphone5Link;
@property (nonatomic) NSString *adMobHtmlSnippet;
@property (nonatomic) NSString *adMobHTMLJSScript;
@end
