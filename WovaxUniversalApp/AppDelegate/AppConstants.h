#define kBaseURL @"http://wovax.co"
#define kApiKey  @"/private_key/"
#define kMenuurl @"menus/get_menu/?menu_id=wovax"
#define kPosturl @"get_recent_posts"
#define kPostDetails @"get_post/?post_id="
#define kpostdetailsType @"get_post/?"
#define ksearch @"get_search_results/?search="
#define kMenuDetails @"get_category_posts/?category_id="
#define kComment @"respond/submit_comment/?"
#define kPostUrlPage @"get_recent_posts/?page="

#define iPhone UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone
#define iPad UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad

#define kAddAnalytics @"options/get_options/?options=wovax_tablet_portrait_banner,wovax_phone_portrait_banner,wovax_link_banner,wovax_ad_option,wovax_html_custom_ad,wovax_js_custom_ad,wovax_admob_type,wovax_admob_key,wovax_google_analytics_id,wovax_excluded_categories"

#define kRegisterRemoteNotiifcation @"/wp-content/plugins/wovaxapp/ios-push-registration.php?deviceId="
#define kClearBadgeToken @"/wp-content/plugins/wovaxapp/ios-clear-badge.php?deviceId="
#define kMap @"/?wovaxmenu=get_map_post"

#define kSegmentHeight 41
#define kSegmentTintColor  [UIColor blueColor]
