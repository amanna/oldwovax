//
//  AppDelegate.h
//  WovaxUniversalApp
//
//  Created by Susim Samanta on 22/09/13.
//  Copyright (c) 2013 Susim Samanta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GADBannerView.h"
#import "GAI.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate,AVAudioPlayerDelegate>{
    bool enableLandscapeMode;
  
}
@property(nonatomic, strong) id<GAITracker> tracker;
@property (strong, nonatomic) UIWindow *window;
+ (AppDelegate *) instance;
-(void)setLandscapeMode:(BOOL)enable;
@property(nonatomic)bool isIpad;
@property(nonatomic)bool isShowingNavLogo;
-(void)trackGoogleAnalyticsWithPageView:(NSString*)page;
@property(nonatomic)NSInteger isPostPage;
@property(nonatomic)UISplitViewController *splitVC;
@end
