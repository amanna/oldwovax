//
//  AppDelegate.m
//  WovaxUniversalApp
//
//  Created by Susim Samanta on 22/09/13.
//  Copyright (c) 2013 Susim Samanta. All rights reserved.
//

#import "AppDelegate.h"
#import "SSMenuVC.h"
#import "SSSliderVC.h"
#import "WebServiceManager.h"
#import "AppConstants.h"
#import "PostDetailsVC.h"
#import "Post.h"
#import "WebViewController.h"
#import "UtiltiyManager.h"
#import "SVWebViewController.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAI.h"
#import "GAIFields.h"
#import "PostMenuListVC.h"
#import "PostDetailsVCIpad.h"

static NSString *const kAllowTracking = @"allowTracking";

@interface AppDelegate ()<UISplitViewControllerDelegate>
@property (nonatomic) SSSliderVC *postVC;
@property (nonatomic) UINavigationController *rootNavigationVC;
@property (nonatomic) PostDetailsVC *postDetails;
@end
@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
//    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
//     BOOL pushOn = [prefs boolForKey:@"push"];
   
    /* if launch after getting push notiifcation */
    if (launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey]) {
        [self application:application didReceiveRemoteNotification:launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey]];
           }
    
   
   

    //[application setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];
    
    
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 80000
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIRemoteNotificationTypeBadge|UIRemoteNotificationTypeSound|UIRemoteNotificationTypeAlert) categories:nil]];
    }else{
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes: (UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge)];
    }
#else
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
     (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
#endif
    
    
    enableLandscapeMode = NO;
    [self trackGoogleAnalyticsWithPageView];
    [self checkDevice];
    return YES;

}
- (void)trackGoogleAnalyticsWithPageView {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *urlString = [NSString stringWithFormat:@"%@%@%@",kBaseURL ,kApiKey,kAddAnalytics];
        NSData *trackerData = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        NSError *jsonError;
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:trackerData options:kNilOptions error:&jsonError];
        if (!jsonError) {
            dispatch_async(dispatch_get_main_queue(), ^(void){
                NSString *trackid = [json objectForKey:@"wovax_google_analytics_id"];
                //
                NSDictionary *appDefaults = @{kAllowTracking: @(YES)};
                [[NSUserDefaults standardUserDefaults] registerDefaults:appDefaults];
                // User must be able to opt out of tracking
                [GAI sharedInstance].optOut =
                ![[NSUserDefaults standardUserDefaults] boolForKey:kAllowTracking];
                // Initialize Google Analytics with a 120-second dispatch interval. There is a
                // tradeoff between battery usage and timely dispatch.
                [GAI sharedInstance].dispatchInterval = 20;
                [GAI sharedInstance].trackUncaughtExceptions = YES;
                // Optional: set Logger to VERBOSE for debug information.
                [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
                self.tracker = [[GAI sharedInstance] trackerWithName:@"CuteAnimals"
                                                          trackingId:trackid];

               
              });
        }
    });
    
}

- (void)checkDevice{
    if (iPad )
    {
        self.isIpad = YES;
        [self initialUISetUpIpad];
    }else{
        self.isIpad = NO;
        [self initialUISetUp];
    }
}
- (void)initialUISetUpIpad{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Ipad" bundle:nil];
    
    UISplitViewController *splitViewController = [storyboard instantiateViewControllerWithIdentifier:@"splitview"];
   // splitViewController = (UISplitViewController *)self.window.rootViewController;
    UINavigationController *detailNavigationController = [splitViewController.viewControllers objectAtIndex:1];
    PostDetailsVCIpad *detailViewController = [detailNavigationController.viewControllers firstObject];
   
    splitViewController.delegate = detailViewController;
    
    UINavigationController *masterNavigationController = [splitViewController.viewControllers firstObject];
    PostMenuListVC *masterViewController = [masterNavigationController.viewControllers firstObject];
    masterViewController.detailViewController = detailViewController;
    [self.window setRootViewController:splitViewController];
    [self.window makeKeyAndVisible];
    
    
    
}
- (void)initialUISetUp {
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    if(self.isIpad){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"StoryboardIPad" bundle:nil];
        self.postVC = [storyboard instantiateViewControllerWithIdentifier:@"SSSliderVC"];
    }else{
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenHeight = screenRect.size.height;
        if(screenHeight > 480){
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
            self.postVC = [storyboard instantiateViewControllerWithIdentifier:@"SSSliderVC"];

        }else{
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"StoryboardSmall" bundle:nil];
            self.postVC = [storyboard instantiateViewControllerWithIdentifier:@"SSSliderVC"];

        }
           }
    __block AppDelegate *blockSelf = self;
   [self.postVC setEventOnCompletion:^(id object, NSUInteger eventType) {
       if (eventType == kActionEvent) {
       }
    else {
           [UIView animateWithDuration:0.4 animations:^{
               CGRect frame = blockSelf.rootNavigationVC.view.frame;
               if(iPad){
                    frame.origin.x = (frame.origin.x  == 0) ?  frame.size.width*0.39 :  0;
               }else{
                    frame.origin.x = (frame.origin.x  == 0) ?  frame.size.width*0.78 :  0;
               }
              
               blockSelf.rootNavigationVC.view.frame = frame;
           }];
       }

   }];
  // [[UITextField appearance] setTintColor:[UIColor blueColor]];
    self.rootNavigationVC  = [[UINavigationController alloc] initWithRootViewController:self.postVC];
    self.rootNavigationVC.navigationBar.barTintColor = [UIColor colorWithRed:248.0/255.0 green:248.0/255.0 blue:248.0/255.0 alpha:1];
    self.rootNavigationVC.navigationBar.tintColor = [UIColor colorWithRed:37.0/255.0 green:126.0/255.0 blue:249.0/255.0 alpha:1];
    [self.rootNavigationVC.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1]}];
    

    
    [[self.rootNavigationVC.view layer] setShadowOffset:CGSizeMake(-2.0f, 0.0f)];
    [[self.rootNavigationVC.view layer] setShadowColor:[UIColor blackColor].CGColor];
    [[self.rootNavigationVC.view layer] setMasksToBounds:NO];
    [[self.rootNavigationVC.view layer] setShadowOpacity:0.2];
    SSMenuVC *menu;
    if(_isIpad){
         UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"StoryboardIPad" bundle:nil];
         menu = [storyboard instantiateViewControllerWithIdentifier:@"SSMenuVC"];
    }else{
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenHeight = screenRect.size.height;
        if(screenHeight > 480){
         UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
         menu = [storyboard instantiateViewControllerWithIdentifier:@"SSMenuVC"];
        }else{
         UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"StoryboardSmall" bundle:nil];
         menu = [storyboard instantiateViewControllerWithIdentifier:@"SSMenuVC"];
        }
    }
   
    CGRect frame1 = self.rootNavigationVC.view.frame;
    frame1.origin.x = 0;
    self.rootNavigationVC.view.frame = frame1;
    [menu setEventOnCompletion:^(id object, NSUInteger eventType) {
        if (eventType == 4) {
            [UIView animateWithDuration:0.4 animations:^{
                CGRect frame = blockSelf.rootNavigationVC.view.frame;
                if(iPad){
                    frame.origin.x = (frame.origin.x  == 0) ?  frame.size.width*0.39 :  0;
                }else{
                     frame.origin.x = (frame.origin.x  == 0) ?  frame.size.width*0.78 :  0;
                }
               
                blockSelf.rootNavigationVC.view.frame = frame;
            }];
            [blockSelf.postVC setPostUIWithSearchText:object];
        }else if(eventType == 0){
            [UIView animateWithDuration:0.4 animations:^{
                CGRect frame = blockSelf.rootNavigationVC.view.frame;
                if(iPad){
                    frame.origin.x = (frame.origin.x  == 0) ?  frame.size.width*0.39 :  0;
                }else{
                    frame.origin.x = (frame.origin.x  == 0) ?  frame.size.width*0.78 :  0;
                }
                blockSelf.rootNavigationVC.view.frame = frame;
            }];
             blockSelf.postVC.posttype = selectByDefault;
            [blockSelf.postVC loadRecentPost];
        }else if (eventType == 3){
            [UIView animateWithDuration:0.4 animations:^{
                CGRect frame = blockSelf.rootNavigationVC.view.frame;
                if(iPad){
                    frame.origin.x = (frame.origin.x  == 0) ?  frame.size.width*0.39 :  0;
                }else{
                    frame.origin.x = (frame.origin.x  == 0) ?  frame.size.width*0.78 :  0;
                }
                blockSelf.rootNavigationVC.view.frame = frame;
            }];
            blockSelf.postVC.posttype = selectByDefault;
            [blockSelf.postVC loadMapView];

        }
        else {
            
            [UIView animateWithDuration:0.4 animations:^{
                CGRect frame = blockSelf.rootNavigationVC.view.frame;
                if(iPad){
                    frame.origin.x = (frame.origin.x  == 0) ?  frame.size.width*0.39 :  0;
                }else{
                    frame.origin.x = (frame.origin.x  == 0) ?  frame.size.width*0.78 :  0;
                }
                blockSelf.rootNavigationVC.view.frame = frame;
            }];
            Menu *obj = object;
            if([obj.menuType isEqualToString:@"custom"]){
               [blockSelf.postVC setPostdetailsCustomWebView:object];

            }else if([obj.menuType isEqualToString:@"category"]){
                blockSelf.postVC.posttype = selectMenuDetails;
                [blockSelf.postVC setUIWithDataSource:object];
            }else {
                [blockSelf.postVC setPostdetails:object];
            }
            
        }
        
    }];
    [menu.view addSubview:self.rootNavigationVC.view];
    self.window.rootViewController = menu;
    [self.window makeKeyAndVisible];
    
}
//-(void)setLandscapeMode:(BOOL)enable{
//    
//    enableLandscapeMode = enable;
//}
//-(NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
//
//{
//    if(enableLandscapeMode == YES)
//    {
//        return UIInterfaceOrientationMaskAllButUpsideDown;
//    }
//    else{
//        return UIInterfaceOrientationMaskPortrait;
//    }
//}


+ (AppDelegate *) instance {
	return (AppDelegate *) [[UIApplication sharedApplication] delegate];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    //[self.postDetails.player performSelector:@selector(play) withObject:nil afterDelay:0.01];
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [GAI sharedInstance].optOut = ![[NSUserDefaults standardUserDefaults] boolForKey:kAllowTracking];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
   
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
#pragma mark -
#pragma mark Push notifications

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken {
    
	NSString* newToken = [deviceToken description];
	newToken = [newToken stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
	newToken = [newToken stringByReplacingOccurrencesOfString:@" " withString:@""];
	NSLog(@"My token is: %@", newToken);
    [WebServiceManager registerRemoteNotiifcationWithDeviceId:newToken onCompletion:^(id object, NSError *error) {
        if (error) {
            NSLog(@"%@",error);
        }else {
            NSLog(@"Device Registered");
        }
    }];
    
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    [UtiltiyManager showAlertWithMessage:error.description];
}
- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo
{
   
    if (userInfo) {
        [UIApplication sharedApplication].applicationIconBadgeNumber += 1;
        NSString *title = [NSString stringWithFormat:@"%@",userInfo[@"aps"][@"alert"]];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Notification" message:title delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        // add redirection code here
        [self doAppRedirectionUsingInfo:userInfo];
    }
    
}
- (void)doAppRedirectionUsingInfo:(NSDictionary *)userInfo {
    NSString *redirectionType = userInfo[@"aps"][@"redirectType"];
    NSString *redirectionValue = userInfo [@"aps"][@"redirectValue"];
    if ([redirectionType isEqualToString:@"post"]) {
        [WebServiceManager fetchPostTypeDetails:redirectionValue withType:@"post" onCompletion:^(id object, NSError *error) {
            if (error) {
                [UtiltiyManager showAlertWithMessage:error.description];
            }else {
                [self goToPostDetails:object];
            }
        }];
    }else if ([redirectionType isEqualToString:@"page"]){
        [WebServiceManager fetchPostTypeDetails:redirectionValue withType:@"page" onCompletion:^(id object, NSError *error) {
            if (error) {
                [UtiltiyManager showAlertWithMessage:error.description];
            }else {
                [self goToPostDetails:object];
            }
        }];
    }else if ([redirectionType isEqualToString:@"link"]) {
        SVWebViewController *webVC = [[SVWebViewController alloc] initWithAddress:redirectionValue];
        webVC.modalPresentationStyle = UIModalPresentationPageSheet;
        [self.rootNavigationVC pushViewController:webVC animated:YES];
    }
}
- (void)goToPostDetails:(id)object {
    Post *post = object;
    PostDetailsVC *postDetails;
    if(!self.isIpad){
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
        postDetails = [story instantiateViewControllerWithIdentifier:@"PostDetailsVC"];
    }else{
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"StoryboardIPad" bundle:nil];
        postDetails = [story instantiateViewControllerWithIdentifier:@"PostDetailsVC"];
    }
    postDetails.post = post;
    [self.rootNavigationVC pushViewController:postDetails animated:YES];
}
#pragma mark Background fetch handler
-(void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler{
//    [WebServiceManager fetchAppsOnCompletion:^(NSArray *fullList, NSError *error) {
//        if (error) {
//            // show error
//            completionHandler(UIBackgroundFetchResultFailed);
//        }else {
//            if ([self.window.rootViewController isKindOfClass:([RootVC class])]) {
//                [UtilityManager archiveAppToLocalFile:fullList];
//                RootVC *rootVC = (RootVC *)self.window.rootViewController;
//                [rootVC refreshUIWithDataSource:[UtilityManager getAppsFromLocalFile]];
//            }
//            completionHandler(UIBackgroundFetchResultNewData);
//        }
//    }];
}

@end
