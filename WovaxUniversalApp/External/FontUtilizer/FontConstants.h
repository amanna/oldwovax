#define HEADER_FONT            [UIFont fontWithName:@"HelveticaNeue" size:22]
#define NORMAL_BOLD_FONT       [UIFont fontWithName:@"HelveticaNeue" size:18]
#define DATE_FONT            [UIFont fontWithName:@"HelveticaNeue" size:14]
#define NORMAL_FONT            [UIFont fontWithName:@"HelveticaNeue" size:14]
#define COLOR_CUSTOM(fltRed , fltGreen , fltBlue)  [UIColor colorWithRed:fltRed/255.0 green:fltGreen/255.0 blue:fltBlue/255.0 alpha:1.0]


#define HEADER_FONT_IPAD       [UIFont fontWithName:@"HelveticaNeue" size:30]
#define DATE_FONT_IPAD            [UIFont fontWithName:@"HelveticaNeue" size:18]
#define NORMAL_FONT_IPAD            [UIFont fontWithName:@"HelveticaNeue" size:18]