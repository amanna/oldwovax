//
//  GoogleAdServiceManager.h
//  WovaxApp
//
//  Created by Susim Samanta on 01/06/13.
//  Copyright (c) 2013 joeharby. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AdModel.h"
@interface GoogleAdServiceManager : NSObject
+ (BOOL)isCreateGoogleAddBanner:(GADBannerView *)bannerView inRootView:(UIViewController *)rootVC;
+ (AdModel *)getAdModelInfo;
@end
