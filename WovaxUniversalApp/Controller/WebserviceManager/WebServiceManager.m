//
//  WebServiceManager.m
//  WovaxUniversalApp
//
//  Created by Susim Samanta on 11/11/13.
//  Copyright (c) 2013 Susim Samanta. All rights reserved.
//

#import "WebServiceManager.h"
#import "SSRestManager.h"
#import "Menu.h"
#import "Post.h"
#import "Menudetails.h"
#import "Search.h"
#import "AppConstants.h"

@implementation WebServiceManager
+(void)fetchWovaxMenusOnCompletion:(FetchCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc] init];
    NSString *menuUrl = [NSString stringWithFormat:@"%@%@",kApiKey,kMenuurl];
    [restManager getJsonResponseFromBaseUrl:kBaseURL query:menuUrl onCompletion:^(NSDictionary *json) {
        if (json) {

                NSArray *menuList = [json valueForKey:@"menu"];
                NSMutableArray *menuFullList = [[NSMutableArray alloc] init];
                [menuList enumerateObjectsUsingBlock:^(NSDictionary *menuDict, NSUInteger idx, BOOL *stop) {
                    Menu *menu=[[Menu alloc]initWithDictionary:menuDict];
                    [menuFullList addObject:menu];
                    
                }];
                
            handler (menuFullList,nil);
        }
    } onError:^(NSError *error) {
        if (error) {
            handler(nil,error);
        }
    }];
}
+ (void)fetchImageDataWithLink:(NSString *)imageLink OnCompletion:(FetchCompletionHandler)handler {
    SSRestManager *restManager = [[SSRestManager alloc] init];
    [restManager getServiceResponseWithBaseUrl:imageLink query:nil onCompletion:^(id responseData, NSURLResponse *reponse) {
        handler (responseData,nil);
    } onError:^(NSError *error) {
        handler (nil,error);
    }];

}
+ (void)fetchRecentPostOnCompletion:(FetchCompletionHandler)handler
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *urlString = [NSString stringWithFormat:@"%@%@%@",kBaseURL ,kApiKey,kAddAnalytics];
        NSData *trackerData = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        NSError *jsonError;
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:trackerData options:kNilOptions error:&jsonError];
        if (!jsonError) {
            dispatch_async(dispatch_get_main_queue(), ^(void){
                NSString *trackid = [json objectForKey:@"wovax_google_analytics_id"];
                SSRestManager *restManager = [[SSRestManager alloc] init];
                NSString *strPost = [NSString stringWithFormat:@"%@%@",kApiKey,kPosturl];
                [restManager getJsonResponseFromBaseUrl:kBaseURL query:strPost onCompletion:^(NSDictionary *json) {
                    if (json) {
                        NSArray *arrPost = [json valueForKey:@"posts"];
                        NSMutableArray *postList = [[NSMutableArray alloc] init];
                        [arrPost enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
                            Post *recent = [[Post alloc]initWithDictionary:dict];
                            //                NSString *str = @",3,5,6,7";
                            //                NSString *leftString = [str substringFromIndex:1];
                            //                NSLog(@"%@--%@",str,leftString);
                            //                NSArray *arr = [leftString componentsSeparatedByString:@","];
                            //                if(![arr containsObject:@"5"]){
                            //                }
                            if(![recent.postHidden isEqualToString:@"1"] && ![recent.postCatId isEqualToString:@"3"]&& ![recent.postCatId isEqualToString:@"5"]){
                                [postList addObject:recent];
                            }
                            
                        }];
                        handler (postList,nil);
                    }
                } onError:^(NSError *error) {
                    if (error) {
                        handler(nil,error);
                    }
                }];

                
                
                
            });
        }
    });

    
    
}

+ (void)fetchPostDetails:(NSString *)strPostId onCompletion:(FetchCompletionHandler)handler
{
    NSString *strPostDetails = [NSString stringWithFormat:@"%@%@",kApiKey,kPostDetails];
    NSString *kQuery = [strPostDetails stringByAppendingString:strPostId];
    SSRestManager *restManager = [[SSRestManager alloc]init];
    [restManager getJsonResponseFromBaseUrl:kBaseURL query:kQuery onCompletion:^(NSDictionary *json) {
        if(json){
            NSDictionary *dictPost = [json valueForKey:@"post"];
           Post *recent = [[Post alloc]initWithDictionary:dictPost];
            handler (recent,nil);
          }
       } onError:^(NSError *error) {
           if (error) {
               handler(nil,error);
           }
        
    }];
}

+ (void)submitComment:(NSString *)strPostId  withText:(NSString*)strComment  onCompletion:(FetchCompletionHandler)handler {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *name = [prefs stringForKey:@"name"];
    name = [name stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    NSString *email = [prefs stringForKey:@"email"];
    NSString *website = [prefs stringForKey:@"website"];
    strComment = [self URLEncodeStringFromString:strComment];
    NSString *strPost = [NSString stringWithFormat:@"post_id=%@&name=%@&url=%@&email=%@&content=%@", strPostId,name,website,email,strComment];
    //NSString *kQuery = [kComment stringByAppendingString:strPost];
    NSString *kQuery = [NSString stringWithFormat:@"%@%@%@%@",kBaseURL,kApiKey,kComment,strPost];
    SSRestManager *restManager = [[SSRestManager alloc]init];
    [restManager getJsonResponseFromBaseUrl:kQuery query:@"" onCompletion:^(NSDictionary *json) {
        if(json){
           handler (@"",nil);
        }
    } onError:^(NSError *error) {
        if (error) {
            handler(nil,error);
        }
    }];
}
+ (NSString *)URLEncodeStringFromString:(NSString *)string {
    static CFStringRef charset = CFSTR("!@#$%&*()+'\";:=,/?[] ");
    CFStringRef str = (__bridge CFStringRef)string;
    CFStringEncoding encoding = kCFStringEncodingUTF8;
    return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, str, NULL, charset, encoding));
}
+ (void)fetchMenuDetails:(NSString*)strMenuId withPage:(NSInteger)page onCompletion:(FetchCompletionHandler)handler{
    NSString *kQuery = [NSString stringWithFormat:@"%@%@%@%@&page=%d",kBaseURL,kApiKey,kMenuDetails,strMenuId,page];
    SSRestManager *restmanger = [[SSRestManager alloc]init];
    [restmanger getJsonResponseFromBaseUrl:kQuery query:kQuery onCompletion:^(NSDictionary *json) {
        if(json){
            NSArray *arrPost = [json valueForKey:@"posts"];
            NSMutableArray *postList = [[NSMutableArray alloc] init];
            [arrPost enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
                Post *recent = [[Post alloc]initWithDictionary:dict];
                [postList addObject:recent];
            }];
            handler (postList,nil);
        }
    } onError:^(NSError *error) {
        handler(nil,error);
    }];
    
}

+ (void)fetchSearchDetails:(NSString *)strSearch onCompletiion:(FetchCompletionHandler)handler
{
    strSearch = [strSearch stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    //NSString *kQuery = [ksearch stringByAppendingString:strSearch];
    NSString *kQuery = [NSString stringWithFormat:@"%@%@%@%@",kBaseURL,kApiKey,ksearch,strSearch];
    SSRestManager *restmanger = [[SSRestManager alloc]init];
    [restmanger getJsonResponseFromBaseUrl:kQuery query:kQuery onCompletion:^(NSDictionary *json) {
        if(json){
            NSArray *arrPost = [json valueForKey:@"posts"];
            NSMutableArray *postList = [[NSMutableArray alloc] init];
            [arrPost enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
                Post *recent = [[Post alloc]initWithDictionary:dict];
                [postList addObject:recent];
            }];
            handler (postList,nil);
        }
    } onError:^(NSError *error) {
        handler(nil,error);
    }];
}
+ (void)registerRemoteNotiifcationWithDeviceId:(NSString *)deviceToken onCompletion:(FetchCompletionHandler)handler {
    NSString *kQuery = [NSString stringWithFormat:@"%@%@%@",kBaseURL,kRegisterRemoteNotiifcation,deviceToken];
    SSRestManager *restmanger = [[SSRestManager alloc]init];
    [restmanger getJsonResponseFromBaseUrl:kQuery query:kQuery onCompletion:^(NSDictionary *json) {
        if(json){
            handler (json,nil);
        }
    } onError:^(NSError *error) {
        handler(nil,error);
    }];
}
+ (void)fetchPostTypeDetails:(NSString *)strPostId withType:(NSString*)strType onCompletion:(FetchCompletionHandler)handler {
    NSString *kQuery = [NSString stringWithFormat:@"%@%@%@post_id=%@&post_type=%@",kBaseURL,kApiKey,kpostdetailsType,strPostId,strType];
    SSRestManager *restManager = [[SSRestManager alloc]init];
    [restManager getJsonResponseFromBaseUrl:kQuery query:kQuery onCompletion:^(NSDictionary *json) {
        if(json){
            NSDictionary *dictPost = [json valueForKey:@"post"];
            Post *recent = [[Post alloc]initWithDictionary:dictPost];
            handler (recent,nil);
        }
    } onError:^(NSError *error) {
        if (error) {
            handler(nil,error);
        }
    }];
}

+ (void)fetchRecentPostPagewiseOnCompletion:(NSInteger)page onCompletion:(FetchCompletionHandler)handler {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *urlString = [NSString stringWithFormat:@"%@%@%@",kBaseURL ,kApiKey,kAddAnalytics];
        NSData *trackerData = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        NSError *jsonError;
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:trackerData options:kNilOptions error:&jsonError];
        if (!jsonError) {
            dispatch_async(dispatch_get_main_queue(), ^(void){
                NSString *excludepost = [json objectForKey:@"wovax_excluded_categories"];
                NSArray *arr;
                if(![excludepost isEqualToString:@""]){
                NSString *leftString = [excludepost substringFromIndex:1];
                arr = [leftString componentsSeparatedByString:@","];
                }
                SSRestManager *restManager = [[SSRestManager alloc] init];
                NSString *strPage = [NSString stringWithFormat:@"%@%@%d",kApiKey,kPostUrlPage,page];
                [restManager getJsonResponseFromBaseUrl:kBaseURL query:strPage onCompletion:^(NSDictionary *json) {
                    if (json) {
                        NSArray *arrPost = [json valueForKey:@"posts"];
                        NSMutableArray *postList = [[NSMutableArray alloc] init];
                        [arrPost enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
                            Post *recent = [[Post alloc]initWithDictionary:dict];
                            if(arr.count > 0){
                                if(![recent.postHidden isEqualToString:@"1"] && ![arr containsObject:recent.postCatId]){
                                [postList addObject:recent];
                                }
                            }else{
                                if(![recent.postHidden isEqualToString:@"1"]){
                                    [postList addObject:recent];
                                }
                            }
                            
                        }];
                        handler (postList,nil);
                        
                        
                        
                    
                    }
                } onError:^(NSError *error) {
                    if (error) {
                        handler(nil,error);
                    }
                }];
                
                
                
                
            });
        }else{
            NSLog(@"%@",jsonError.debugDescription);
        }
    });

}
+ (void)callClearBadgeTokenServiceOncompletion:(FetchCompletionHandler)handler {
    SSRestManager *restManager = [[SSRestManager alloc] init];
    NSString *menuUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,kClearBadgeToken];
    [restManager getJsonResponseFromBaseUrl:kBaseURL query:menuUrl onCompletion:^(NSDictionary *json) {
        if (json) {
            handler (nil,nil);
        }
    } onError:^(NSError *error) {
        if (error) {
            handler(nil,error);
        }
    }];
}
+ (void)callMapService:(FetchCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc] init];
    [restManager getJsonResponseFromBaseUrl:kBaseURL query:kMap onCompletion:^(NSDictionary *json) {
        if (json) {
            NSString *satuts = [json valueForKey:@"status"];
            if([satuts isEqualToString:@"OK"]){
                 NSArray *arrPost = [json valueForKey:@"post"];
                 NSDictionary *dict = [arrPost objectAtIndex:0];
                 Post *recent = [[Post alloc]init];
                 recent.postTitle = [dict valueForKey:@"title"];
                 recent.postLongitude = [dict valueForKey:@"geo_longitude"];
                 recent.postLatt = [dict valueForKey:@"geo_latitude"];
                 handler (recent,nil);
            }
            
        }
    } onError:^(NSError *error) {
        if (error) {
            handler(nil,error);
        }
    }];

}
@end
