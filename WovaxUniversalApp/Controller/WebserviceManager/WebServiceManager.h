//
//  WebServiceManager.h
//  WovaxUniversalApp
//
//  Created by Susim Samanta on 11/11/13.
//  Copyright (c) 2013 Susim Samanta. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^FetchCompletionHandler)(id object, NSError *error);
@interface WebServiceManager : NSObject
+(void)fetchWovaxMenusOnCompletion:(FetchCompletionHandler)handler;
+ (void)fetchImageDataWithLink:(NSString *)imageLink OnCompletion:(FetchCompletionHandler)handler;
+ (void)fetchRecentPostOnCompletion:(FetchCompletionHandler)handler;
+ (void)fetchPostDetails:(NSString *)strPostId onCompletion:(FetchCompletionHandler)handler;
+ (void)fetchMenuDetails:(NSString*)strMenuId withPage:(NSInteger)page onCompletion:(FetchCompletionHandler)handler;
+ (void)fetchSearchDetails:(NSString *)strSearch onCompletiion:(FetchCompletionHandler)handler;
+ (void)submitComment:(NSString *)strPostId  withText:(NSString*)strComment  onCompletion:(FetchCompletionHandler)handler;
+ (void)registerRemoteNotiifcationWithDeviceId:(NSString *)deviceToken onCompletion:(FetchCompletionHandler)handler;
+ (void)fetchPostTypeDetails:(NSString *)strPostId withType:(NSString*)strType onCompletion:(FetchCompletionHandler)handler;
+ (void)fetchRecentPostPagewiseOnCompletion:(NSInteger)page onCompletion:(FetchCompletionHandler)handler;
+ (NSString *)URLEncodeStringFromString:(NSString *)string;
+ (void)callClearBadgeTokenServiceOncompletion:(FetchCompletionHandler)handler;
+ (void)callMapService:(FetchCompletionHandler)handler;
@end


