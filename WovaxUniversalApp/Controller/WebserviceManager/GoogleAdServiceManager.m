//
//  GoogleAdServiceManager.m
//  WovaxApp
//
//  Created by Susim Samanta on 01/06/13.
//  Copyright (c) 2013 joeharby. All rights reserved.
//
#import "AppDelegate.h"
#import "GoogleAdServiceManager.h"
#import "GADAdSize.h"
#import "AppConstants.h"
#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
@implementation GoogleAdServiceManager

+ (BOOL)isCreateGoogleAddBanner:(GADBannerView *)bannerView inRootView:(UIViewController *)rootVC
{
    NSString *adUnitID;
    NSString *adPostiion;
    NSString *urlString = [NSString stringWithFormat:@"%@",kAddAnalytics];
    NSData *responseData = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
    NSError *error;
    id json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    if (!error) {
        adUnitID = [json objectForKey:@"wovax_admob_key"];
        adPostiion = [json objectForKey:@"wovax_admob_type"];
        GADAdSize adsize;
        if ([adPostiion isEqualToString:@"0"]) {
            return NO;
        }else if ([adPostiion isEqualToString:@"1"]) {
            adsize = kGADAdSizeBanner;
        }else {
                // [[AppDelegate instance] refreshAdMobBanner];
            adsize = kGADAdSizeBanner;
        }
        bannerView = [[GADBannerView alloc] initWithAdSize: adsize];
        bannerView.adUnitID = adUnitID;
        bannerView.rootViewController = rootVC;
        bannerView.frame = IS_IPHONE_5 ? CGRectMake(0, 0, 320, 50) : CGRectMake(0, 0, 320, 50);
        [rootVC.view addSubview:bannerView];
        GADRequest *request = [GADRequest request];
        request.testDevices = [NSArray arrayWithObjects:@"GAD_SIMULATOR_ID", nil];
        [bannerView loadRequest:request];
    }else {
        NSLog(@"Json error%@",error);
        return NO;
    }
    return YES;
}
+ (AdModel *)getAdModelInfo {
    AdModel *adModel = [[AdModel alloc] init];
//    NSString *urlString = [NSString stringWithFormat:@"%@/wp-content/plugins/wovaxapp/ipad/wovaxAppSelectAd.php",kBaseURL];
     NSString *urlString = [NSString stringWithFormat:@"%@%@%@",kBaseURL,kApiKey,kAddAnalytics];
    NSData *responseData = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
    NSError *error;
    id json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    if(!error) {
        NSString *adType = [json objectForKey:@"wovax_ad_option"];
        if ([adType isEqualToString:@"adMob"]) {
            adModel.adMobType = kGoogleAdMob;
        }else if([adType isEqualToString:@"upload_image_link"]) {
            adModel.adMobType = kOnlyImageBanner;
            NSString *urlString = [NSString stringWithFormat:@"%@/wp-content/plugins/wovaxapp/ipad/wovaxAppImageAd.php",kBaseURL];
            NSData *responseData = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
            NSError *error;
            id json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
            if(!error) {
                adModel.adMobImageLink = [json objectForKey:@"topBannerImage"];
                adModel.adMobWebViewLink = [json objectForKey:@"webViewImageLink"];
                adModel.adMobFullImageLink = [json objectForKey:@"webViewImage"];
                adModel.adMobFullImageIphone5Link = [json objectForKey:@"webViewImageLinkIphone5"];
            }
        }else if([adType isEqualToString:@"upload_html_snippet"]) {
            adModel.adMobType = kHTMLSnippet;
//            NSString *urlString = [NSString stringWithFormat:@"%@/wp-content/plugins/wovaxapp/ipad/wovaxAppHtmlAd.php",kBaseURL];
//            NSData *responseData = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
//            NSError *error;
//            id json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
            
                NSDictionary *dataDictionary=json;
                adModel.adMobHtmlSnippet = [dataDictionary objectForKey:@"wovax_html_custom_ad"];
                adModel.adMobHTMLJSScript = [dataDictionary objectForKey:@"wovax_js_custom_ad"];
                adModel.adMobHtmlSnippet = [adModel.adMobHtmlSnippet stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                 adModel.adMobHtmlSnippet = [ adModel.adMobHtmlSnippet stringByReplacingOccurrencesOfString:@"+" withString: @" "];
                
                adModel.adMobHTMLJSScript = [adModel.adMobHTMLJSScript stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                adModel.adMobHTMLJSScript = [ adModel.adMobHTMLJSScript stringByReplacingOccurrencesOfString:@"+" withString: @" "];

            
        }else if([adType isEqualToString:@"banner_image"]) {
            adModel.adMobType = kSmallImageBanner;
            if(iPhone){
            adModel.adMobImageLink = [NSString stringWithFormat:@"%@/wp-content/plugins/wovaxapp/wovax/%@",kBaseURL,[json objectForKey:@"wovax_phone_portrait_banner"]];
            }else{
                 adModel.adMobImageLink = [NSString stringWithFormat:@"%@/wp-content/plugins/wovaxapp/wovax/%@",kBaseURL,[json objectForKey:@"wovax_tablet_portrait_banner"]];
            }
            adModel.adMobWebViewLink = [json objectForKey:@"wovax_link_banner"];
        }else {
            adModel.adMobType = kNone;
        }
    }
    return adModel;
}
@end
