//
//  SafariActivity.h
//  WovaxApp
//
//  Created by Susim Samanta on 12/08/13.
//  Copyright (c) 2013 joeharby. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^OpenSafariCompletion)();
@interface SafariActivity : UIActivity {
    OpenSafariCompletion openSafariCompletionHandler;
}
- (void)opennInSafariOnCompletion:(OpenSafariCompletion)handler;
@end
