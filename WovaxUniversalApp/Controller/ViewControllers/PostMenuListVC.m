//
//  PostMenuListTableViewController.m
//  WovaxUniversalApp
//
//  Created by Amrita on 08/09/14.
//  Copyright (c) 2014 Susim Samanta. All rights reserved.
//

#import "PostMenuListVC.h"
#import "WebServiceManager.h"
#import "UtiltiyManager.h"
#import "StorageManager.h"
#import "Post.h"
#import "Menu.h"
#import "PostDetailsVCIpad.h"
@interface PostMenuListVC (){
    UISegmentedControl *segmentedControl;
    UISearchBar *search;
}
@property (strong,nonatomic) NSMutableArray* dataSource;
@property (nonatomic)NSInteger prevpage;
@property (nonatomic)NSInteger currpage;

@property (strong, nonatomic) IBOutlet UITableView *tblView;
@property(nonatomic)BOOL isMenu;
@end

@implementation PostMenuListVC

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.dataSource = [[NSMutableArray alloc]init];
    self.isMenu = NO;
     [self loadRecentPost];
    
   
}
- (void)loadRecentPost{
    if ([UtiltiyManager isReachableToInternet]) {
        self.currpage = 1;
        [WebServiceManager fetchRecentPostPagewiseOnCompletion:self.currpage onCompletion:^(id object, NSError *error) {
            if(error){
                [UtiltiyManager showAlertWithMessage:@"Server unresponsive. Please try again."];
            }else{
                
                self.prevpage = self.currpage;
                self.currpage= self.currpage +  1;
                self.dataSource = object;
                Post *post = [self.dataSource objectAtIndex:0];
                self.detailViewController.post = post;
                [self.detailViewController loadPostDetails];
                [StorageManager savePostList:self.dataSource];
                [self.tblView reloadData];
            }
        }];
        
        
    }else {
        self.dataSource = [StorageManager getPostList];
        [self.tblView reloadData];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    return self.dataSource.count + 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    if(indexPath.row==0){
        cell.textLabel.text = @"Home";
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor colorWithRed:86.0/255.0 green:86.0/255.0 blue:86.0/255.0 alpha:1];
        cell.textLabel.textColor = [UIColor whiteColor];
    }else{
    // Configure the cell...
        cell.backgroundColor = [UIColor clearColor];
    if(self.isMenu==YES){
        Menu *menu = [self.dataSource objectAtIndex:indexPath.row-1];
        cell.textLabel.text = menu.menuName;
    }else{
         Post *post = [self.dataSource objectAtIndex:indexPath.row-1];
        cell.textLabel.text = post.postTitle;
    }
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0){
        self.isMenu = YES;
        [self fetchMenuList];
    }else{
    if(self.isMenu == YES){
        Menu *menu = [self.dataSource objectAtIndex:indexPath.row-1];
        if([menu.menuType isEqualToString:@"category"]){
            [self setMenudetailsCat:menu];
        }else if ([menu.menuType isEqualToString:@"post"]){
            [self setMenudetailsPost:menu];
        }else if ([menu.menuType isEqualToString:@"page"]){
             [self setMenudetailsPost:menu];
        }
        
    }else{
        Post *post = [self.dataSource objectAtIndex:indexPath.row-1];
        self.detailViewController.post = post;
        [self.detailViewController loadPostDetails];
    }
    }
    
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if(self.isMenu == NO){
    float offs = (scrollView.contentOffset.y+scrollView.bounds.size.height);
    float val = (scrollView.contentSize.height);
    if ((int)offs > (int)val)
    {
        if(self.currpage != self.prevpage){
            self.prevpage = self.currpage;
            [WebServiceManager fetchRecentPostPagewiseOnCompletion:self.currpage onCompletion:^(id object, NSError *error) {
                if(error){
                    [UtiltiyManager showAlertWithMessage:@"Server unresponsive. Please try again."];
                }else{
                    NSMutableArray *newPost = [[NSMutableArray alloc]init];
                    newPost = object;
                    [newPost enumerateObjectsUsingBlock:^(Post *post, NSUInteger idx, BOOL *stop) {
                        [self.dataSource addObject:post];
                    }];
                    self.currpage= self.currpage +  1;
                    [StorageManager savePostList:newPost];
                    [self.tblView reloadData];
                    // [self loadData];
                    
                }
            }];
        }

    }
    }
}
- (void)setMenudetailsPost:(Menu*)object{
    [WebServiceManager fetchPostTypeDetails:object.menuObjectId withType:object.menuType onCompletion:^(id object, NSError *error) {
        if(!error){
            Post *post = object;
            self.detailViewController.post = post;
            [self.detailViewController loadPostDetails];
            
            
        }
    }];
}

- (void)setMenudetailsCat:(Menu *)object {
    if ([UtiltiyManager isReachableToInternet]) {
        //self.currpageDetails = 1;
        //self.menu1 = object;
        [WebServiceManager fetchMenuDetails:object.menuObjectId withPage:1 onCompletion:^(id object, NSError *error) {
            if (error) {
                [UtiltiyManager showAlertWithMessage:@"Server unresponsive. Please try again."];
            }else {
                //self.prevpageDetails = self.currpageDetails;
                //self.currpageDetails= self.currpageDetails +  1;
                self.dataSource = object;
                [self.tblView reloadData];
                self.isMenu = NO;
                [StorageManager savePostList:self.dataSource];
                //[self loadData];
            }
        }];
    }else {
        self.dataSource = [StorageManager getPostList];
        //[self loadData];
    }
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *aHeaderView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 100)];
    [aHeaderView setBackgroundColor:[UIColor clearColor]];
   NSArray *itemArray = [NSArray arrayWithObjects: @"Posts", @"Menu",nil];
   segmentedControl = [[UISegmentedControl alloc] initWithItems:itemArray];
   segmentedControl.frame = CGRectMake(50, 5, 220, kSegmentHeight);
   ///segmentedControl.segmentedControlStyle = UISegmentedControlStylePlain;
    segmentedControl.tintColor = kSegmentTintColor;
   [segmentedControl addTarget:self action:@selector(postMenuSegmentAction:) forControlEvents: UIControlEventValueChanged];
    if(self.isMenu==NO){
         segmentedControl.selectedSegmentIndex = 0;
    }else{
         segmentedControl.selectedSegmentIndex = 1;
    }
   [aHeaderView addSubview:segmentedControl];
   tableView.tableHeaderView = aHeaderView;
    
     search = [[UISearchBar alloc]init];
     UIDeviceOrientation interfaceOrientation = [[UIDevice currentDevice] orientation];
    if(interfaceOrientation==UIDeviceOrientationPortrait){
        search.frame = CGRectMake(0, 65, 308, 40);
    }else{
        search.frame = CGRectMake(0, 65, 308, 40);
    }
    [aHeaderView addSubview:search];
    return aHeaderView;
}
- (void)postMenuSegmentAction:(UISegmentedControl *)segment
{
    if(segment.selectedSegmentIndex == 0)
    {
        // code for the first button
        segmentedControl.selectedSegmentIndex = 0;
        self.isMenu = NO;
        [self loadRecentPost];
    }else{
         segmentedControl.selectedSegmentIndex = 1;
        self.isMenu = YES;
        [self fetchMenuList];
    }
}
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    switch (orientation) {
        case 1:
        case 2:
            NSLog(@"portrait");
            search.frame = CGRectMake(2, 65, 304, 40);
            break;
            
        case 3:
        case 4:
            NSLog(@"landscape");
             search.frame = CGRectMake(0, 65, 300, 40);
            // your code for landscape...
            break;
        default:
            NSLog(@"other");
            // your code for face down or face up...
            break;
    }
}
- (void)fetchMenuList {
    if ([UtiltiyManager isReachableToInternet]) {
        [WebServiceManager fetchWovaxMenusOnCompletion:^(id object, NSError *error) {
            if(!error){
                self.dataSource=object;
                [StorageManager saveMenuList:self.dataSource];
                [self.tblView reloadData];
            }else {
                [UtiltiyManager showAlertWithMessage:@"Server unresponsive. Please try again."];
            }
        }];
    }else {
        self.dataSource = [StorageManager getMenuList];
        [self.tblView reloadData];
    }
    
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
