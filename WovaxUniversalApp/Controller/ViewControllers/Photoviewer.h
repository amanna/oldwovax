//
//  Photoviewer.h
//  WovaxUniversalApp
//
//  Created by Amrita on 25/06/14.
//  Copyright (c) 2014 Susim Samanta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Post.h"
#import "MWPhotoBrowser.h"
@interface Photoviewer : UIViewController<MWPhotoBrowserDelegate>
@property(nonatomic)Post *post;
@end
