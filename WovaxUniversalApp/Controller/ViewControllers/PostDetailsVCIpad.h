//
//  PostDetailsVCIpad.h
//  WovaxUniversalApp
//
//  Created by Amrita on 08/09/14.
//  Copyright (c) 2014 Susim Samanta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Post.h"
@interface PostDetailsVCIpad : UIViewController<UISplitViewControllerDelegate,UIPopoverControllerDelegate>
@property (nonatomic, strong) NSString * detailItem;
@property (nonatomic) Post *post;
-(void)loadPostDetails;
@end
