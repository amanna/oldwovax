//
//  SSSliderVC.h
//  SSMenuSlider
//
//  Created by Susim on 11/6/13.
//  Copyright (c) 2013 Susim. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SSMenuVC.h"
#import "GAITrackedViewController.h"
typedef NS_ENUM(NSUInteger, strPostType) {
    selectRecent=1,
    selectMenuDetails,
    selectByDefault
};
@interface SSSliderVC : GAITrackedViewController <UIGestureRecognizerDelegate>{
    EventCompletionHandler eventCompletionHandler;
}
- (void)setUIWithDataSource:(id)object;
- (void)setPostUIWithSearchText:(NSString *)searchText;
- (void)setEventOnCompletion:(EventCompletionHandler )handler;
- (void)setPostdetails:(id)object;
- (void)setPostdetailsCustomWebView:(id)object;
- (void)setRecentPosts;
- (void)loadRecentPost;
- (void)loadMapView;
@property (nonatomic,assign) strPostType posttype;
@end
