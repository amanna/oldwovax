//
//  PostDetailsVCViewController.m
//  WovaxUniversalApp
//
//  Created by Amrita on 19/12/13.
//  Copyright (c) 2013 Susim Samanta. All rights reserved.
//

#import "PostDetailsVC.h"
#import "PostDetailsCell.h"
#import "WebServiceManager.h"
#import "Post.h"
#import "UtiltiyManager.h"
#import "AppConstants.h"
#import <Social/Social.h>
#import "SVWebViewController.h"
#import "SDImageCache.h"
#import "MWCommon.h"
#import "AppDelegate.h"
#import "SVModalWebViewController.h"
#import "WebViewController.h"
#import "MapVC.h"
#import "SettingsVC.h"
#import "SettingsVCIPad.h"
#import "GoogleAdServiceManager.h"
#import "AdModel.h"
#import "HTMLAd.h"
#import "GAI.h"
#import <AVFoundation/AVFoundation.h>
#define kOFFSET_FOR_KEYBOARD 216
@interface PostDetailsVC()<HPGrowingTextViewDelegate,UIGestureRecognizerDelegate>{
    BOOL isEditing;
   
}
@property(nonatomic)NSMutableArray *photos;
@property(nonatomic)NSMutableArray *thumbs;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property(nonatomic)Post *datasource;
@property (nonatomic) AdModel *adModel;
@end

@implementation PostDetailsVC
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    BOOL ok;
    NSError *setCategoryError = nil;
    ok = [audioSession setCategory:AVAudioSessionCategoryPlayback
                             error:&setCategoryError];
    if (!ok) {
        NSLog(@"%s setCategoryError=%@", __PRETTY_FUNCTION__, setCategoryError);
    }
    self.webView.mediaPlaybackAllowsAirPlay = YES;
        [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];

    [self setPlayBackNotiifcation];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadPostDetails)
                                                 name:@"refreshNotification"
                                               object:nil];

   // self.view.backgroundColor = [UIColor redColor];
    [self loadPostDetails];
    UIBarButtonItem *actionBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(actionBtnClicked)];
    
     if([self.post.postMapEnable isEqualToString:@"1"]){
    UIBarButtonItem *mapButton = [[UIBarButtonItem alloc]
                                     initWithImage:[UIImage imageNamed:@"723-location-arrow.png"]
                                     style:UIBarButtonItemStylePlain
                                     target:self action:@selector(goToMapView)];
   
   self.navigationItem.rightBarButtonItems = [[NSArray alloc] initWithObjects:actionBtn, mapButton, nil];
       
     }else{
          self.navigationItem.rightBarButtonItem = actionBtn;
     }
    

    [self setGoogleAdevrtisement];
    if(![self.post.commentStatus isEqualToString:@"open"]){
       
        CGRect frame1 = self.webView.frame;
        frame1.size.height = frame1.size.height + 40;
        self.webView.frame = frame1;
    }else{
        [self createAGrowTextView];
    }
    self.screenName = self.post.postTitle;
    //AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //[appDel trackGoogleAnalyticsWithPageView:self.screenName];
}
- (BOOL)canBecomeFirstResponder {
    return YES;
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.screenName = self.post.postTitle;
}


- (void)goToMapView{
    MapVC *mapVC;
    if(iPhone){
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenHeight = screenRect.size.height;
        if(screenHeight > 480){
            UIStoryboard *story = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
            mapVC = [story instantiateViewControllerWithIdentifier:@"MapVC"];
            mapVC.post = self.post;
        }else{
            UIStoryboard *story = [UIStoryboard storyboardWithName:@"StoryboardSmall" bundle:nil];
            mapVC = [story instantiateViewControllerWithIdentifier:@"MapVC"];
            mapVC.post = self.post;
        }
    
   }else{
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"StoryboardIPad" bundle:nil];
    mapVC = [story instantiateViewControllerWithIdentifier:@"MapVC"];
    mapVC.post = self.post;
   }
    [self.navigationController pushViewController:mapVC animated:YES];
}
    

- (void)setPlayBackNotiifcation {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playbackStateDidChange:)
                                                 name:@"MPAVControllerPlaybackStateChangedNotification"
                                               object:nil];
}
- (void)playbackStateDidChange:(NSNotification *)note
{
   
    NSLog(@"note.name=%@ state=%d", note.name, [[note.userInfo objectForKey:@"MPAVControllerNewStateParameter"] intValue]);
    int playbackState = [[note.userInfo objectForKey:@"MPAVControllerNewStateParameter"] intValue];
    switch (playbackState) {
        case 1: //end
            [[AppDelegate instance] setLandscapeMode:NO];
            break;
        case 2: //start
        [[AppDelegate instance] setLandscapeMode:YES];
            break;
        default:
            break;
    }
}

- (void)actionBtnClicked{
    NSArray *objectsToShare;
    NSString *text = self.post.postTitle;
    NSURL *url = [NSURL URLWithString:self.post.postUrl];
    if(![self.post.postType isEqualToString:@"page"]){
    NSURL *imageUrl = [NSURL URLWithString:self.post.postThumb];
    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:imageUrl]];
        if(image==nil){
            objectsToShare = @[text, url];
        }else{
             objectsToShare = @[text, url,image];
        }
   
    }else{
         objectsToShare = @[text, url];
    }
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypePostToFacebook
                                  ];
    
    activityVC.excludedActivityTypes = excludeActivities;
    [self presentViewController:activityVC animated:YES completion:nil];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString *padding = @"document.body.style.margin='0';document.body.style.padding = '0'";
    [webView stringByEvaluatingJavaScriptFromString:padding];
    if([self.post.postGalleryEnable isEqualToString:@"1"]){
    NSString *gallery = @"function getJson() { $('#get_post_gallery').load('$baseurl$/?wovaxmenu=get_post_gallery&id=$postid$'); } getJson();";
    gallery = [gallery stringByReplacingOccurrencesOfString:@"$postid$" withString:self.post.postId];
    gallery = [gallery stringByReplacingOccurrencesOfString:@"$baseurl$" withString:kBaseURL];
    [self.webView stringByEvaluatingJavaScriptFromString:gallery];
       
    }
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)aRequest navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        NSString *myString = [[aRequest URL] absoluteString];
        NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"http://photo"] invertedSet];
        if([myString rangeOfCharacterFromSet:set].location != NSNotFound){
            NSArray *arys = [myString componentsSeparatedByString:@";"];
            if([arys count] == 1)
            {
                NSLog(@"WebView");
                NSString *lastThreeChars = [myString substringFromIndex:myString.length-3];
                if ([lastThreeChars isEqualToString:@"jpg"] || [lastThreeChars isEqualToString:@"png"]) {
//                    EGOPhotoViewController *photoController = [[EGOPhotoViewController alloc] initWithImageURL:[NSURL URLWithString:myString]];
//                    [[AppDelegate instance] setLandscapeMode:YES];
//                    [self.navigationController pushViewController:photoController animated:YES];
                }else {
                    
                    [self gotONewWebView:myString];
                }
                return NO;
            }
            else
            {
                if([self.post.postGalleryEnable isEqualToString:@"1"]){
                    
                    // push new webview with image ...
                    [self goToBrowsePhoto];
                }
                return YES;
            }
             return YES;
        }
        return YES;
        // check if user clicked on your image...clickedOnImageWithUrl is made up
   }
     return YES;
  
}
- (void)gotONewWebView:(NSString*)myString{
    NSString *str = [NSString stringWithFormat:@"%@",myString];
    SVWebViewController *objSettingsVC = [[SVWebViewController alloc]initWithAddress:str];
    objSettingsVC.modalPresentationStyle = UIModalPresentationPageSheet;
   [self.navigationController pushViewController:objSettingsVC animated:YES];
}
- (void)goToBrowsePhoto{
    // Create array of MWPhoto objects
    self.photos = [NSMutableArray array];
    self.thumbs = [NSMutableArray array];
    // Photos
    __block MWPhoto *photo;
   
    BOOL displayNavArrows = NO;
    BOOL startOnGrid = NO;
    
    
    for (NSDictionary *dict in self.post.postAttachment) {
        NSDictionary *dictImage = [dict valueForKey:@"images"];
        NSDictionary *fulldict = [dictImage valueForKey:@"full"];
        NSString *strImageUrl = [NSString stringWithFormat:@"%@",[fulldict valueForKey:@"url"]];
        photo = [MWPhoto photoWithURL:[NSURL URLWithString:strImageUrl]];
        [self.photos addObject:photo];
    }
    // Thumbs
    for (NSDictionary *dict in self.post.postAttachment) {
        NSDictionary *dictImage = [dict valueForKey:@"images"];
        NSDictionary *fulldict = [dictImage valueForKey:@"thumbnail"];
        NSString *strImageUrl = [NSString stringWithFormat:@"%@",[fulldict valueForKey:@"url"]];
        photo = [MWPhoto photoWithURL:[NSURL URLWithString:strImageUrl]];
        [self.thumbs addObject:photo];
    }
    
    startOnGrid = YES;
    displayNavArrows = YES;
    
    
    // Create browser (must be done each time photo browser is
    // displayed. Photo browser objects cannot be re-used)
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    
    // Set options
    browser.displayActionButton = YES; // Show action button to allow sharing, copying, etc (defaults to YES)
    browser.displayNavArrows = NO; // Whether to display left and right nav arrows on toolbar (defaults to NO)
    browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
    browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
    browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
    browser.enableGrid = YES; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
    browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
    //browser.wantsFullScreenLayout = YES; // iOS 5 & 6 only: Decide if you want the photo browser full screen, i.e. whether the status bar is affected (defaults to YES)
    
    // Optionally set the current visible photo before displaying
    [browser setCurrentPhotoIndex:1];
    // Manipulate
    [self.navigationController pushViewController:browser animated:YES];
    [browser showNextPhotoAnimated:YES];
    [browser showPreviousPhotoAnimated:YES];
    [browser setCurrentPhotoIndex:0];
    

}
#pragma mark - MWPhotoBrowserDelegate
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return self.photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < self.photos.count)
        return [self.photos objectAtIndex:index];
    return nil;
}
- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser thumbPhotoAtIndex:(NSUInteger)index {
    if (index < _thumbs.count)
        return [_thumbs objectAtIndex:index];
    return nil;
}
- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser didDisplayPhotoAtIndex:(NSUInteger)index {
    NSLog(@"Did start viewing photo at index %lu", (unsigned long)index);
}


//- (NSString *)photoBrowser:(MWPhotoBrowser *)photoBrowser titleForPhotoAtIndex:(NSUInteger)index {
//    return [NSString stringWithFormat:@"Photo %lu", (unsigned long)index+1];
//}


- (void)photoBrowserDidFinishModalPresentation:(MWPhotoBrowser *)photoBrowser {
    // If we subscribe to this method we must dismiss the view controller ourselves
    NSLog(@"Did finish modal presentation");
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)loadPostDetails {
    NSString *path;
    if([self.post.postType isEqualToString:@"page"]){
        if([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
            path = [[NSBundle mainBundle] pathForResource:@"detailspageIpad" ofType:@"html"];
        }else{
            path = [[NSBundle mainBundle] pathForResource:@"detailspage" ofType:@"html"];
       }
    }else{
        if([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
            path = [[NSBundle mainBundle] pathForResource:@"detailspostIpad" ofType:@"html"];
        }else{
            path = [[NSBundle mainBundle] pathForResource:@"detailspost" ofType:@"html"];
        }
        
    }
    
    NSString *html = [[NSString alloc] initWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    html = [html stringByReplacingOccurrencesOfString:@"$baseurl$" withString:kBaseURL];
    html = [html stringByReplacingOccurrencesOfString:@"$title$" withString:self.post.postTitle];
    html = [html stringByReplacingOccurrencesOfString:@"$date$" withString:self.post.postDate];
    html = [html stringByReplacingOccurrencesOfString:@"$name$" withString:self.post.postBy];
    html = [html stringByReplacingOccurrencesOfString:@"$image$" withString:self.post.postThumb?self.post.postThumb : @"" ];
    html = [html stringByReplacingOccurrencesOfString:@"$content$" withString:self.post.postContent];
    html = [html stringByReplacingOccurrencesOfString:@"$post_id$" withString:self.post.postId];
    
    [self.webView loadHTMLString:html baseURL:nil];
   
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)resignTextView{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    NSString *name = [prefs stringForKey:@"name"];
    NSString *email = [prefs stringForKey:@"email"];
    if(name==nil){
        [UtiltiyManager showAlertWithMessage:@"Please enter your details on the settings page"];
    }else if(email==nil){
        [UtiltiyManager showAlertWithMessage:@"Please enter your details on the settings page"];
    }else{
        if([textView.text isEqualToString:@""]){
              [UtiltiyManager showAlertWithMessage:@"Please enter comment"];
        }
        else{
            if([textView.text isEqualToString:@"Post a comment"]){
                textView.text = @"";
               [UtiltiyManager showAlertWithMessage:@"Please enter comment"];
            }else{
        [WebServiceManager submitComment:self.post.postId withText:textView.text onCompletion:^(id object, NSError *error) {
            if(!error){
                [textView resignFirstResponder];
                [UtiltiyManager showAlertWithMessage:@"Your comment was posted successfully. Your comment may be held for moderation."];
                 [self performSelectorOnMainThread:@selector(reloadcomment) withObject:nil waitUntilDone:NO];
               
            }
        }];
            }
        }
    }

}
- (void)reloadcomment{
     [self.webView stringByEvaluatingJavaScriptFromString:@"reloadComment();"];
}
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [self setViewMovedUp:YES];
}
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
        
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    
}
- (void)createAGrowTextView {
	if(iPhone){
         containerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 40, 320, 40)];
    }else{
        containerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 40, 768, 40)];
    }
   
    containerView.backgroundColor = [UIColor clearColor];
    if(iPhone){
        textView = [[HPGrowingTextView alloc] initWithFrame:CGRectMake(41, 3, 205, 40)];
    }else{
        textView = [[HPGrowingTextView alloc] initWithFrame:CGRectMake(41, 3, 653, 40)];
    }
	
    textView.contentInset = UIEdgeInsetsMake(0, 5, 0, 5);
    
	textView.minNumberOfLines = 1;
	textView.maxNumberOfLines = 3;
	textView.returnKeyType = UIReturnKeyDefault;
	textView.font = [UIFont systemFontOfSize:15.0f];
	textView.delegate = self;
    textView.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(5, 0, 5, 0);
    textView.backgroundColor = [UIColor whiteColor];
    //textView.text = NSLocalizedString(@"fullstory_comment_placeholder", nil);
    textView.text = @"Post a comment";
    textView.textColor = [UIColor grayColor];
   [textView setTintColor:[UIColor colorWithRed:6/255.0 green:135/255.0 blue:235/255.0 alpha:1.0]];
   [self.view addSubview:containerView];
	
    UIImage *rawEntryBackground = [UIImage imageNamed:@"MessageEntryInputField.png"];
    UIImage *entryBackground = [rawEntryBackground stretchableImageWithLeftCapWidth:13 topCapHeight:22];
    UIImageView *entryImageView = [[UIImageView alloc] initWithImage:entryBackground];
    
    if(iPhone){
        entryImageView.frame = CGRectMake(40, 0, 213, 40);
    }else{
        entryImageView.frame = CGRectMake(40, 0, 653, 40);
    }
    
    

    entryImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    UIImage *rawBackground = [UIImage imageNamed:@"MessageEntryBackground.png"];
    UIImage *background = [rawBackground stretchableImageWithLeftCapWidth:13 topCapHeight:22];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:background];
    imageView.frame = CGRectMake(0, 0, containerView.frame.size.width, containerView.frame.size.height);
    imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    textView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    // view hierachy
    [containerView addSubview:imageView];
    [containerView addSubview:textView];
    [containerView addSubview:entryImageView];
    
    UIImage *sendBtnBackground = [[UIImage imageNamed:@"MessageEntrySendButton.png"] stretchableImageWithLeftCapWidth:13 topCapHeight:0];
    UIImage *selectedSendBtnBackground = [[UIImage imageNamed:@"MessageEntrySendButton.png"] stretchableImageWithLeftCapWidth:13 topCapHeight:0];
    
    UIImage *settingBtnBackground = [[UIImage imageNamed:@"MessageEntrySettingButton.png"] stretchableImageWithLeftCapWidth:13 topCapHeight:0];
    UIImage *selectedSettingSendBtnBackground = [[UIImage imageNamed:@"MessageEntrySettingButton.png"] stretchableImageWithLeftCapWidth:13 topCapHeight:0];
    
	UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
	doneBtn.frame = CGRectMake(containerView.frame.size.width - 69, 8, 63, 27);
    doneBtn.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin;
    [doneBtn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:13]];
	//[doneBtn setTitle:NSLocalizedString(@"fullstory_comment_post_btn", nil) forState:UIControlStateNormal];
    [doneBtn setTitle:@"Post" forState:UIControlStateNormal];
    
    [doneBtn setTitleShadowColor:[UIColor colorWithWhite:0 alpha:0.4] forState:UIControlStateNormal];
    doneBtn.titleLabel.shadowOffset = CGSizeMake (0.0, 0.0);
    doneBtn.titleLabel.font = [UIFont boldSystemFontOfSize:17];
    
    [doneBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
	[doneBtn addTarget:self action:@selector(resignTextView) forControlEvents:UIControlEventTouchUpInside];
    [doneBtn setBackgroundImage:sendBtnBackground forState:UIControlStateNormal];
    [doneBtn setBackgroundImage:selectedSendBtnBackground forState:UIControlStateSelected];
	[containerView addSubview:doneBtn];
    
    
    
    UIButton *setBtn = [UIButton buttonWithType:UIButtonTypeCustom];
	setBtn.frame = CGRectMake(6, 8, 27, 27);
    setBtn.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin;
    [setBtn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:13]];
	
    
    [setBtn setTitleShadowColor:[UIColor colorWithWhite:0 alpha:0.4] forState:UIControlStateNormal];
    setBtn.titleLabel.shadowOffset = CGSizeMake (0.0, -1.0);
    setBtn.titleLabel.font = [UIFont boldSystemFontOfSize:13];
    
    [setBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[setBtn addTarget:self action:@selector(openSetting) forControlEvents:UIControlEventTouchUpInside];
    [setBtn setBackgroundImage:settingBtnBackground forState:UIControlStateNormal];
    [setBtn setBackgroundImage:selectedSettingSendBtnBackground forState:UIControlStateSelected];
	[containerView addSubview:setBtn];
    
    
    
    containerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
}
-(void)openSetting
{
    if(iPad){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"StoryboardIPad" bundle:nil];
        SettingsVCIPad *objSettingsVC = [storyboard instantiateViewControllerWithIdentifier:@"SettingsVCIPad"];
        [self.navigationController pushViewController:objSettingsVC animated:YES];
    }else{
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenHeight = screenRect.size.height;
        if(screenHeight > 480){
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
            SettingsVC *objSettingsVC = [storyboard instantiateViewControllerWithIdentifier:@"SettingsVC"];
            [self.navigationController pushViewController:objSettingsVC animated:YES];
        }else{
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"StoryboardSmall" bundle:nil];
            SettingsVC *objSettingsVC = [storyboard instantiateViewControllerWithIdentifier:@"SettingsVC"];
            [self.navigationController pushViewController:objSettingsVC animated:YES];
        }
    }
}
//Code from Brett Schumann
-(void) keyboardWillShow:(NSNotification *)note{
    isEditing = YES;
    // get keyboard size and loctaion
	CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    // Need to translate the bounds to account for rotation.
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    
	// get a rect for the textView frame
	CGRect containerFrame = containerView.frame;
    containerFrame.origin.y = self.view.bounds.size.height - (keyboardBounds.size.height + containerFrame.size.height);
	// animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
	
	// set views with new info
	containerView.frame = containerFrame;
	textView.text = @"";
    textView.textColor = [UIColor blackColor];
	// commit animations
	[UIView commitAnimations];
    CGRect frame = self.webView.frame;
    frame.size.height = self.webView.frame.size.height - 216;
    self.webView.frame = frame;
    
    CGPoint bottomOffset = CGPointMake(0, self.webView.scrollView.contentSize.height - self.webView.scrollView.bounds.size.height);
    [self.webView.scrollView setContentOffset:bottomOffset animated:YES];
}

-(void) keyboardWillHide:(NSNotification *)note{
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
	
	// get a rect for the textView frame
	CGRect containerFrame = containerView.frame;
    containerFrame.origin.y = self.view.bounds.size.height - containerFrame.size.height;
	
	// animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
	// set views with new info
	containerView.frame = containerFrame;
	
	// commit animations
	[UIView commitAnimations];
    textView.placeholder = @"Post a comment";
   
    textView.textColor = [UIColor grayColor];
    CGRect frame = self.webView.frame;
    frame.size.height = self.webView.frame.size.height + 216;
    self.webView.frame = frame;
    
    CGPoint topOffset = CGPointMake(0, 0);
    [self.webView.scrollView setContentOffset:topOffset animated:YES];
}

- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    float diff = (growingTextView.frame.size.height - height);
    
	CGRect r = containerView.frame;
    r.size.height -= diff;
    r.origin.y += diff;
	containerView.frame = r;
}

#pragma mark Google advertisement
- (void)setGoogleAdevrtisement {
    self.adModel = [GoogleAdServiceManager getAdModelInfo];
    [self setAdvertisement];
}
- (void)setAdvertisement {
    
    if (self.adModel.adMobType == kSmallImageBanner) {
        UIButton *addButton = [UIButton buttonWithType:UIButtonTypeCustom];
        addButton.frame =   CGRectMake(0, 64, 320, 50);
        UIImage *image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.adModel.adMobImageLink]]];
        [addButton setBackgroundImage:image forState:UIControlStateNormal];
        [addButton addTarget:self
                      action:@selector(adSmallBannerImageTapped:)
            forControlEvents:(UIControlEvents)UIControlEventTouchDown];
        [self.view addSubview:addButton];
         self.webView.frame = CGRectMake(0, 50, 320, 479);
       
    }else if (self.adModel.adMobType==kHTMLSnippet){
        UIWebView *myWebView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 64, 320, 50)];
        myWebView.scalesPageToFit = YES;
        NSString *html = self.adModel.adMobHtmlSnippet;
        NSString *javascript = self.adModel.adMobHTMLJSScript;
        html= [html stringByAppendingString:javascript];
        [myWebView loadHTMLString:html baseURL:nil];
        [self.view addSubview:myWebView];
        [self addTapGestureInView:myWebView];
        self.webView.frame = CGRectMake(0, 50, 320, 479);
    }
    
}
- (void)addTapGestureInView :(UIWebView *)webView {
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnAd:)];
    tapGesture.numberOfTapsRequired = 1;
    webView.userInteractionEnabled = YES;
    tapGesture.delegate = self;
    [webView addGestureRecognizer:tapGesture];
}
- (void)tapOnAd :(id)sender {
    HTMLAd *htmlAd = [[HTMLAd alloc] init];
    NSString *html = self.adModel.adMobHtmlSnippet;
    NSString *javascript = self.adModel.adMobHTMLJSScript;
    html= [html stringByAppendingString:javascript];
    htmlAd.htmlSnippet = html;
    /*if (!isShowingFullScreenAdd) {
     isShowingFullScreenAdd = YES;
     UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:htmlAd];
     [self presentViewController:navigationController animated:YES completion:NULL];
     }*/
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}
- (void)adSmallBannerImageTapped:(id)sender {
    //    HTMLAd *htmlAd = [[HTMLAd alloc] init];
    //    htmlAd.isSmallImageBanner = YES;
    //    htmlAd.webViewImageLink = self.adModel.adMobWebViewLink;
    
    SVWebViewController *newLink = [[SVWebViewController alloc]initWithAddress:self.adModel.adMobWebViewLink];
    [self.navigationController pushViewController:newLink animated:YES];
    //    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:htmlAd];
    //    [self presentViewController:navigationController animated:YES completion:NULL];
}
- (void)adBannerImageTapped:(id)sender {
    HTMLAd *htmlAd = [[HTMLAd alloc] init];
   BOOL IS_IPHONE_5 = ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON );
    htmlAd.buttonImageLink = IS_IPHONE_5 ?  self.adModel.adMobFullImageIphone5Link : self.adModel.adMobFullImageLink;
    htmlAd.webViewImageLink = self.adModel.adMobWebViewLink;
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:htmlAd];
    [self presentViewController:navigationController animated:YES completion:NULL];
}
- (void)adBannerTapped:(id)sender {
    HTMLAd *htmlAd = [[HTMLAd alloc] init];
    htmlAd.buttonImageLink = self.adModel.adMobImageLink;
    htmlAd.webViewImageLink = self.adModel.adMobWebViewLink;
    /*if (!isShowingFullScreenAdd) {
     isShowingFullScreenAdd = YES;
     UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:htmlAd];
     [self presentViewController:navigationController animated:YES completion:NULL];
     }*/
}


@end
