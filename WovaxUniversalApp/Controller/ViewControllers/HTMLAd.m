//
//  HTMLAd.m
//  WovaxApp
//
//  Created by Susim Samanta on 24/07/13.
//  Copyright (c) 2013 joeharby. All rights reserved.
//

#import "HTMLAd.h"
#import "SafariActivity.h"
#define OS_VERSION ([UIDevice currentDevice].systemVersion)

@interface HTMLAd ()
@property (nonatomic,weak) IBOutlet UIWebView *webView;
@property (nonatomic,weak) IBOutlet UIButton *imageBtn;
- (IBAction)imageButtonTapped:(id)sender;
@end

@implementation HTMLAd

- (void)viewDidLoad {
    [super viewDidLoad];
    if ([OS_VERSION intValue] > 5.0) {
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(share:)];
    }
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(close)];
    if (self.htmlSnippet) {
        [self.webView loadHTMLString:self.htmlSnippet baseURL:nil];
    }else if (self.isSmallImageBanner) {
        [self.imageBtn setHidden:YES];
        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.webViewImageLink]]];
    }
    else {
        UIImage *image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.buttonImageLink]]];
        [self.imageBtn setBackgroundImage:image forState:UIControlStateNormal];
        self.imageBtn.frame =  CGRectMake(0, 0, 320, 505) ;
    }
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:104.0/255.0 green:104.0/255.0 blue:104.0/255.0 alpha:1];
}
-(void)share:(id)sender {
    NSString *postText = @"" ;
    UIImage *image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.webViewImageLink]]];
    NSArray *activityItems;
    if (image) {
        activityItems = @[postText,image];
    }else {
        activityItems = @[postText];
    }
    
    SafariActivity *activity = [[SafariActivity alloc] init];
    [activity opennInSafariOnCompletion:^{
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.webViewImageLink]];
    } ];
    NSArray *applicationActivities = [[NSArray alloc] initWithObjects:activity, nil];
    UIActivityViewController *activityController =
    [[UIActivityViewController alloc]
     initWithActivityItems:activityItems applicationActivities:applicationActivities];
    [self presentViewController:activityController
                       animated:YES completion:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (void)close {
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (IBAction)imageButtonTapped:(id)sender {
    [self.imageBtn setHidden:YES];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.webViewImageLink]]];
}
@end
