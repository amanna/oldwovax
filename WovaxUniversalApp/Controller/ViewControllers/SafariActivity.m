//
//  SafariActivity.m
//  WovaxApp
//
//  Created by Susim Samanta on 12/08/13.
//  Copyright (c) 2013 joeharby. All rights reserved.
//

#import "SafariActivity.h"

@implementation SafariActivity
- (NSString *)activityType{
    return @"SafariActivity";
}
- (NSString *)activityTitle;{
    return @"Safari";
}
- (UIImage *)activityImage{
    UIImage *mp3Icon = [UIImage imageNamed:@"Safari.png"];
    return mp3Icon;
}
- (BOOL)canPerformWithActivityItems:(NSArray *)activityItems
{
    NSLog(@"%s", __FUNCTION__);
    return YES;
}

- (void)prepareWithActivityItems:(NSArray *)activityItems
{
    NSLog(@"%s",__FUNCTION__);
}

- (UIViewController *)activityViewController
{
    NSLog(@"%s",__FUNCTION__);
    return nil;
}

- (void)performActivity
{
    // This is where you can do anything you want, and is the whole reason for creating a custom
    // UIActivity and UIActivityProvider
    NSLog(@"Email MP3");
    if (openSafariCompletionHandler) {
        openSafariCompletionHandler();
    }
    [self activityDidFinish:YES];
}
- (void)opennInSafariOnCompletion:(OpenSafariCompletion)handler {
    openSafariCompletionHandler = handler;
}
@end
