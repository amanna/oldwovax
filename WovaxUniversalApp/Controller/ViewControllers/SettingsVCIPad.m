//
//  SettingsVCIPad.m
//  WovaxUniversalApp
//
//  Created by Amrita on 16/07/14.
//  Copyright (c) 2014 Susim Samanta. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "SettingsVCIPad.h"
#import "SettingsCellTableViewCell.h"
#import "SettingsVC.h"
#import "UtiltiyManager.h"
@interface SettingsVCIPad ()<UITextFieldDelegate>{
    
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *roundedView;
@property (weak, nonatomic) IBOutlet UIButton *btnApplinked;
- (IBAction)appLinkedAction:(id)sender;

@end

@implementation SettingsVCIPad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidAppear:(BOOL)animated {
    self.screenName =  @"Settings";
    [super viewDidAppear:animated];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7){
        self.tableView.contentInset = UIEdgeInsetsMake(-65, 0, 0, 0);
    }
   // self.tableView.contentInset = UIEdgeInsetsZero;
    [self.tableView setBackgroundView:nil];
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    [self.tableView.layer setCornerRadius:5.0];
    [self.roundedView.layer setCornerRadius:5.0];
    [self setUpSettingsBar];
    
    self.btnApplinked.tintColor =[UIColor colorWithRed:.1725 green:0.53 blue:.98 alpha:1.0];
    
   }

    // Do any additional setup after loading the view.

- (void)setUpSettingsBar
{
    self.navigationItem.title = @"Settings";
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleBordered target:self action:@selector(btnDoneClickedAction)];
    self.navigationItem.rightBarButtonItem = rightBarButton;
    self.navigationItem.rightBarButtonItem.tintColor = [UIColor colorWithRed:.1725 green:0.53 blue:.98 alpha:1.0];
}
- (void)btnDoneClickedAction{
    UITextField *txtName = (UITextField*)[self.view viewWithTag:4000];
     UITextField *txtEmail = (UITextField*)[self.view viewWithTag:4001];
     UITextField *txtWebsite = (UITextField*)[self.view viewWithTag:4002];
   if([txtName.text isEqualToString:@""] ){
       [UtiltiyManager showAlertWithMessage:@"Please enter your name."];
   }else if ([txtEmail.text isEqualToString:@""]){
       [UtiltiyManager showAlertWithMessage:@"Please enter your email."];
   }else{
       BOOL isValidEmail = [self validateEmail:txtEmail.text];
       if(isValidEmail){
           NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
           [prefs setObject:txtName.text forKey:@"name"];
           [prefs setObject:txtEmail.text forKey:@"email"];
           [prefs setObject:txtWebsite.text forKey:@"website"];
           [self.navigationController popViewControllerAnimated:YES];
       }else{
           [UtiltiyManager showAlertWithMessage:@"Please enter a valid email."];
       }
   }
    
}
-(BOOL)validateEmail: (NSString *) candidate {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex]; //  return 0;
    return [emailTest evaluateWithObject:candidate];
}
#pragma mark Table View Data Source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return 3;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    SettingsCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[SettingsCellTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    NSString *name = [prefs stringForKey:@"name"];
    NSString *email = [prefs stringForKey:@"email"];
    NSString *website = [prefs stringForKey:@"website"];
    
 
    if(indexPath.row==0){
        cell.lblName.text = @"Name";
        cell.lblText.tag = 4000;
        cell.lblText.text = name;
        cell.lblText.autocapitalizationType = UITextAutocapitalizationTypeWords;
        
    }else if (indexPath.row==1){
        cell.lblName.text = @"Email";
         cell.lblText.tag = 4001;
         cell.lblText.text = email;
        cell.lblText.keyboardType = UIKeyboardTypeEmailAddress;
    }else{
        cell.lblName.text = @"Website";
         cell.lblText.text = website;
         cell.lblText.tag = 4002;
         cell.lblText.keyboardType = UIKeyboardTypeURL;
        cell.separatorInset = UIEdgeInsetsMake(0, cell.bounds.size.width, 0, 0);
    }
    cell.lblText.delegate = self;
    return cell;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 43;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section==0){
         return 0.0;
    }
    return 0.0;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UILabel *sectionHeader = [[UILabel alloc] initWithFrame:CGRectNull];
    sectionHeader.hidden = YES;
    return sectionHeader;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if(section==0){
        return 0;
    }
    return 0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - UITextField  delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    UITextField *txtName = (UITextField*)[self.view viewWithTag:4000];
    UITextField *txtEmail = (UITextField*)[self.view viewWithTag:4001];
    UITextField *txtWebsite = (UITextField*)[self.view viewWithTag:4002];
    if(textField == txtName){
        [txtName resignFirstResponder];
        [txtEmail becomeFirstResponder];
    }else if (textField == txtEmail){
        [txtEmail resignFirstResponder];
        [txtWebsite becomeFirstResponder];
    }else{
        [txtWebsite resignFirstResponder];
    }
    return YES;
}

- (IBAction)appLinkedAction:(id)sender {
}
@end
