//
//  WebViewController.h
//  WovaxUniversalApp
//
//  Created by Amrita on 21/06/14.
//  Copyright (c) 2014 Susim Samanta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController
@property(nonatomic)NSString *urlAddress;
@end
