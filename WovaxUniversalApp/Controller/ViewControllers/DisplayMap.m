//
//  DisplayMap.m
//  MapKitDisplay
//
//  Created by Chakra on 12/07/10.
//  Copyright 2010 Chakra Interactive Pvt Ltd. All rights reserved.
//

#import "DisplayMap.h"


@implementation DisplayMap

- (id)initWithTitle:(NSString*)newTitle Location:(CLLocationCoordinate2D)location{
    self = [super init];
    if(self){
        title = newTitle;
        coordinate = location;
    }
    return self;
}
@end
