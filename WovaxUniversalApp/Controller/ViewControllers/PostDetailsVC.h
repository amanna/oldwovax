//
//  PostDetailsVCViewController.h
//  WovaxUniversalApp
//
//  Created by Amrita on 19/12/13.
//  Copyright (c) 2013 Susim Samanta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Post.h"
#import "HPGrowingTextView.h"
#import "MWPhotoBrowser.h"
#import "GAITrackedViewController.h"
#import <AVFoundation/AVFoundation.h>
@interface PostDetailsVC : GAITrackedViewController<MWPhotoBrowserDelegate>{
     UIView *containerView;
     HPGrowingTextView *textView;
}
@property(nonatomic) NSString *strPostId;
@property (nonatomic) Post *post;

@end
