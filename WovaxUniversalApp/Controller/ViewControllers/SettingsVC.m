//
//  SettingsVC.m
//  WovaxUniversalApp
//
//  Created by Amrita on 26/02/14.
//  Copyright (c) 2014 Susim Samanta. All rights reserved.
//

#import "SettingsVC.h"
#import "UtiltiyManager.h"
#import "WebViewController.h"
#import "SVWebViewController.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"
#import "AppConstants.h"
@interface SettingsVC ()<UITextFieldDelegate>
- (IBAction)btnDoneClickedAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtWebsite;
- (IBAction)btnBackClickedAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *scroll;

@property (strong, nonatomic) IBOutlet UITableView *tblView;
@property (weak, nonatomic) IBOutlet UILabel *lblTap;
@property (weak, nonatomic) IBOutlet UISwitch *switchSettings;

@end

@implementation SettingsVC
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (IBAction)goToWovax:(id)sender {
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
     id tracker = [[GAI sharedInstance] defaultTracker];
    
    // This screen name value will remain set on the tracker and sent with
    // hits until it is set to a new value or to nil.
    [tracker set:kGAIScreenName
           value:@"Settings"];
    // manual screen tracking
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setUpSettingsBar];
    if (!iPad ){
    [self.tblView setSeparatorInset:UIEdgeInsetsZero];
    }
    self.scroll.contentSize = CGSizeMake(self.scroll.frame.size.width, 600);
    self.txtName.delegate = self;
    self.txtEmail.delegate = self;
    self.txtWebsite.delegate = self;
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *name = [prefs stringForKey:@"name"];
    NSString *email = [prefs stringForKey:@"email"];
    NSString *website = [prefs stringForKey:@"website"];
    BOOL pushOn = [prefs boolForKey:@"push"];
    [self.switchSettings setOn:pushOn];
    self.txtName.text = name;
    self.txtEmail.text =  email;
    self.txtWebsite.text = website;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 100.0f;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(iPad){
   
    }
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 3)];
    headerView.backgroundColor = [UIColor clearColor];   // use your own design
    
    return headerView;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *footer = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 100)];
    if(iPad){
        footer.frame = CGRectMake(0, 0, 768, 100);
    }
    footer.backgroundColor=[UIColor clearColor];
    
    UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(8,0,170,40)];
    if(iPad){
        lbl.frame = CGRectMake(240, 0, 200, 100);
    }
    lbl.backgroundColor = [UIColor clearColor];
    lbl.text = @"App by Woväx, LLC.";
    lbl.textAlignment = NSTextAlignmentCenter;
    [lbl setNumberOfLines:10];//set line if you need
    [lbl setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:14.0]];//font size and style
    [lbl setTextColor:[UIColor blackColor]];
    
    UILabel *lblTap = [[UILabel alloc]initWithFrame:CGRectMake(162,0,150,40)];
    if(iPad){
        lblTap.frame = CGRectMake(430, 0, 200, 100);
    }
    lblTap.text = @"Want your own app?";
    lblTap.textColor = [UIColor colorWithRed:.1725 green:0.53 blue:.98 alpha:1.0];
    [lblTap setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:14.0]];
    [footer addSubview:lblTap];
    [footer addSubview:lbl];
    UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userTappedOnLink)];
    // if labelView is not set userInteractionEnabled, you must do so
    [lblTap setUserInteractionEnabled:YES];
    [lblTap addGestureRecognizer:gesture];
    self.tblView.tableFooterView=footer;
    self.tblView.tableFooterView.userInteractionEnabled = YES;
    return footer;
}
- (void)userTappedOnLink
{
    NSString *str = [NSString stringWithFormat:@"%@",@"https://wovax.com"];
    NSURL *strUrl = [NSURL URLWithString:str];
    SVWebViewController *objSettingsVC = [[SVWebViewController alloc]initWithURL:strUrl];
    [self.navigationController pushViewController:objSettingsVC animated:YES];
}
- (void)setUpSettingsBar
{
    self.navigationItem.title = @"Settings";
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleBordered target:self action:@selector(btnDoneClickedAction)];
    self.navigationItem.rightBarButtonItem = rightBarButton;
    }
- (void)BackToPost{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)btnDoneClickedAction{
    if([self.txtName.text isEqualToString:@""] ){
        [UtiltiyManager showAlertWithMessage:@"Please enter your name"];
    }else if ([self.txtEmail.text isEqualToString:@""]){
        [UtiltiyManager showAlertWithMessage:@"Please enter your email"];
    }else{
        BOOL isValidEmail = [self validateEmail:self.txtEmail.text];
        if(isValidEmail){
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            [prefs setObject:self.txtName.text forKey:@"name"];
            [prefs setObject:self.txtEmail.text forKey:@"email"];
            [prefs setObject:self.txtWebsite.text forKey:@"website"];
            [prefs setBool:self.switchSettings.isOn forKey:@"push"];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
             [UtiltiyManager showAlertWithMessage:@"Please enter valid email"];
        }
    }
    
}
-(BOOL)validateEmail: (NSString *) candidate {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex]; //  return 0;
    return [emailTest evaluateWithObject:candidate];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnDoneClickedAction:(id)sender {
//    if([self.txtEmail.text isEqualToString:@""] || [self.txtName.text isEqualToString:@""]){
//        [UtiltiyManager showAlertWithMessage:@"Please enter name or email"];
//    }
    
   
}
- (IBAction)btnBackClickedAction:(id)sender {
   // [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - UITextField  delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField == self.txtName){
        [self.txtName resignFirstResponder];
        [self.txtEmail becomeFirstResponder];
    }else if (textField == self.txtEmail){
        [self.txtEmail resignFirstResponder];
        [self.txtWebsite becomeFirstResponder];
    }else{
        [self.txtWebsite resignFirstResponder];
    }
    return YES;
}
@end
