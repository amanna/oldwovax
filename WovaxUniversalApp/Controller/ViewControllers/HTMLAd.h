//
//  HTMLAd.h
//  WovaxApp
//
//  Created by Susim Samanta on 24/07/13.
//  Copyright (c) 2013 joeharby. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HTMLAd : UIViewController
@property (nonatomic) NSString *buttonImageLink;
@property (nonatomic) NSString *webViewImageLink;
@property (nonatomic) NSString *htmlSnippet;
@property (nonatomic,assign) BOOL isSmallImageBanner;
@end
