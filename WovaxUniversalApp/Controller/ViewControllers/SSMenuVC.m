//
//  SSMenuVC.m
//  SSMenuSlider
//
//  Created by Susim on 11/6/13.
//  Copyright (c) 2013 Susim. All rights reserved.
//

#import "SSMenuVC.h"
#import "WebServiceManager.h"
#import "Menu.h"
#import "AppConstants.h"
#import "UtiltiyManager.h"
#import "PostDetailsVC.h"
#import "StorageManager.h"
#import "PostDetailsVC.h"
#import "Post.h"
#import "AppDelegate.h"
#import <MessageUI/MessageUI.h>
#import "MapVC.h"
@interface SSMenuVC ()<MFMailComposeViewControllerDelegate>{
    UIRefreshControl *refreshControl;
    AppDelegate *appDelegate;
    MFMailComposeViewController *mailComposer;
}
@property (strong,nonatomic) NSArray* dataSource;
@property (weak, nonatomic) IBOutlet UITableView *tblMenu;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@end

@implementation SSMenuVC
#pragma mark Data Source
- (void)setUIWithDataSource:(id)object {
    
}
- (void)setEventOnCompletion:(EventCompletionHandler)handler {
    eventCompletionHandler = handler;
}
#pragma mark view life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.searchBar.tintColor = [UIColor colorWithRed:.376f green:.386f blue:.452f alpha:1.0];
    [self fetchMenuList];
     refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(sortArray) forControlEvents:UIControlEventValueChanged];
    [self.tblMenu addSubview:refreshControl];

}
- (void)sortArray{
    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self fetchMenuList];
       [refreshControl endRefreshing];
    });
}
- (void)fetchMenuList {
    if ([UtiltiyManager isReachableToInternet]) {
        [WebServiceManager fetchWovaxMenusOnCompletion:^(id object, NSError *error) {
            if(!error){
                self.dataSource=object;
                [StorageManager saveMenuList:self.dataSource];
                [self.tblMenu reloadData];
            }else {
                [UtiltiyManager showAlertWithMessage:@"Server unresponsive. Please try again."];
            }
        }];
    }else {
        self.dataSource = [StorageManager getMenuList];
        [self.tblMenu reloadData];
    }

}
#pragma mark Memory Warnings

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark Table View Data Source 
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return self.dataSource.count + 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    if(indexPath.row==0){
        cell.textLabel.text = @"Home";
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor colorWithRed:86.0/255.0 green:86.0/255.0 blue:86.0/255.0 alpha:1];
        cell.textLabel.textColor = [UIColor whiteColor];
    }else{
        Menu *menu = self.dataSource[indexPath.row - 1];
        cell.textLabel.text = menu.menuName;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
   
    if(self.lastIndexPath!=nil){
        if ([indexPath compare:self.lastIndexPath] == NSOrderedSame)
        {
            cell.backgroundColor = [UIColor colorWithRed:86.0/255.0 green:86.0/255.0 blue:86.0/255.0 alpha:1];
            cell.textLabel.textColor = [UIColor whiteColor];
        }
        else
        {
            cell.backgroundColor = [UIColor whiteColor];
            cell.textLabel.textColor = [UIColor blackColor];
        }
    }    return cell;
    
    }
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    
    return view;
}

#pragma mark Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"%d",indexPath.row);
    Menu *object;
    UITableViewCell *cell =(UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath] ;
    for (int i=0; i<self.dataSource.count; i++) {
        if(i==indexPath.row){
            cell.backgroundColor = [UIColor blackColor];
            cell.textLabel.textColor = [UIColor whiteColor];
        }else{
            cell.backgroundColor = [UIColor whiteColor];
            cell.textLabel.textColor = [UIColor blackColor];
        }
    }
    
    if(indexPath.row !=0){
     object = self.dataSource[indexPath.row - 1];
    if([object.menuType isEqualToString:@"page"] || [object.menuType isEqualToString:@"custom"]){
        if( self.lastIndexRow!=0){
            NSIndexPath *indexPathPrev = [NSIndexPath indexPathForRow:self.lastIndexRow inSection:0];
            self.lastIndexPath = indexPathPrev;
            
        }else{
            NSIndexPath *indexPathPrev = [NSIndexPath indexPathForRow:0 inSection:0];
            self.lastIndexPath = indexPathPrev;
        }
        
    }else{
        self.lastIndexRow = indexPath.row;
        self.lastIndexPath = indexPath;
    }
    }else{
        self.lastIndexRow = indexPath.row;
        self.lastIndexPath = indexPath;
    }
    
    [tableView reloadData];
    
    if(indexPath.row==0){
        if (eventCompletionHandler) {
            eventCompletionHandler(nil,0);
        }
    }else{
        if([object.menuType isEqualToString:@"custom"]){
            if([object.menuCustomType isEqualToString:@"6"]){
                [self sendMail:object.menuEmail];
            }else if ([object.menuCustomType isEqualToString:@"5"]){
                [self callPhone:object.menuPhone];
            }else{
                if (eventCompletionHandler) {
                    eventCompletionHandler(object,3);
                }
            }
        }else{
            if (eventCompletionHandler) {
                eventCompletionHandler(object,5);
            }
        }
    }
    
  
}
- (void)callPhone:(NSString *)strPhone{
    
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"%@",strPhone]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    } else
    {
       UIAlertView *calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [calert show];
    }
}
-(void)sendMail:(NSString*)strEmail{
    mailComposer = [[MFMailComposeViewController alloc]init];
    mailComposer.mailComposeDelegate = self;
    [ mailComposer setToRecipients:[NSArray arrayWithObjects:strEmail,nil]];
    [self presentViewController:mailComposer animated:YES completion:nil];
    
}
     
#pragma mark - mail compose delegate
-(void)mailComposeController:(MFMailComposeViewController *)controller
             didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
                 if (result) {
                     NSLog(@"Result : %d",result);
                 }
                 if (error) {
                     NSLog(@"Error : %@",error);
                 }
    [self dismissViewControllerAnimated:YES completion:nil];
                 
}
- (UITableViewCellAccessoryType)tableView:(UITableView *)tv accessoryTypeForRowWithIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellAccessoryDisclosureIndicator;
    
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    searchBar.showsCancelButton = YES;
    
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    searchBar.showsCancelButton = NO;
    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    NSLog(@"Cancel");
    [searchBar resignFirstResponder];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    if (eventCompletionHandler) {
        eventCompletionHandler(searchBar.text,4);
    }
}
@end
