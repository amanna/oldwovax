//
//  SSSliderVC.m
//  SSMenuSlider
//
//  Created by Susim on 11/6/13.
//  Copyright (c) 2013 Susim. All rights reserved.
//

#import "SSSliderVC.h"
#import "WebServiceManager.h"
#import "AppConstants.h"
#import "Post.h"
#import "CustomPostListCell.h"
#import "UtiltiyManager.h"
#import "PostDetailsVC.h"
#import "AppDelegate.h"
#import "SettingsVC.h"
#import "CustomPostListCell.h"
#import "CustomPostListCell2.h"
#import "CustomPostListCell3.h"
#import "CustomPostListCell4.h"
#import "StorageManager.h"
#import "GoogleAdServiceManager.h"
#import "AdModel.h"
#import "HTMLAd.h"
#import "WebViewController.h"
#import "SVWebViewController.h"

#import "GADBannerView.h"
#import "MapVC.h"
#define kLogoImageName  @"company_logo"
@interface SSSliderVC ()<UIGestureRecognizerDelegate,UIWebViewDelegate>{
    UIRefreshControl *refreshControl;
    AppDelegate *appDelegate;
}
@property (weak, nonatomic) IBOutlet UITableView *tblPost;
- (IBAction)goToMenu:(id)sender;
@property (strong,nonatomic) NSMutableArray* dataSource;
@property (nonatomic) AdModel *adModel;
@property (nonatomic)NSInteger prevpage;
@property (nonatomic)NSInteger currpage;
@property (nonatomic)NSInteger prevpageDetails;
@property (nonatomic)NSInteger currpageDetails;

@property (nonatomic)Menu *menu1;
@end

@implementation SSSliderVC
- (void)viewWillAppear:(BOOL)animated{
    self.navigationItem.title = @"Development";
}
- (void)viewDidAppear:(BOOL)animated {
    self.screenName =  @"Development";
    [super viewDidAppear:animated];
}
- (void)setPostUIWithSearchText:(NSString *)searchText {
    [WebServiceManager fetchSearchDetails:searchText onCompletiion:^(id object, NSError *error) {
        self.dataSource = object;
        [self loadData];
    }];
}
- (void)loadMapView{
    [WebServiceManager callMapService:^(id object, NSError *error) {
        if(object){
            Post *post = object;
            UIStoryboard *story = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
            MapVC *map = [story instantiateViewControllerWithIdentifier:@"MapVC"];
            map.post = post;
            [self.navigationController pushViewController:map animated:YES];
        }else{
            [UtiltiyManager showAlertWithMessage:@"Some Problem"];
        }
    }];

}
- (void)setPostdetails:(Menu*)object{
       [WebServiceManager fetchPostTypeDetails:object.menuObjectId withType:object.menuType onCompletion:^(id object, NSError *error) {
           if(!error){
               Post *post = object;
               PostDetailsVC *postDetails;
               if(!appDelegate.isIpad){
                   CGRect screenRect = [[UIScreen mainScreen] bounds];
                   CGFloat screenHeight = screenRect.size.height;
                   if(screenHeight > 480){
                   UIStoryboard *story = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
                    postDetails = [story instantiateViewControllerWithIdentifier:@"PostDetailsVC"];
                   }else{
                       UIStoryboard *story = [UIStoryboard storyboardWithName:@"StoryboardSmall" bundle:nil];
                       postDetails = [story instantiateViewControllerWithIdentifier:@"PostDetailsVC"];
                   }
               }
               postDetails.post = post;
               [self.navigationController pushViewController:postDetails animated:YES];
           }
       }];
}

- (void)setPostdetailsCustomWebView:(Menu*)object
{
    NSString *str = [NSString stringWithFormat:@"%@",object.menuUrl];
    NSURL *strUrl = [NSURL URLWithString:str];
    SVWebViewController *objSettingsVC  =  [[SVWebViewController alloc] initWithURL:strUrl];
    [self.navigationController pushViewController:objSettingsVC animated:YES];
   
}
-(void)setRecentPosts
{
    if ([UtiltiyManager isReachableToInternet]) {
        [WebServiceManager fetchRecentPostOnCompletion:^(id object, NSError *error) {
            if(error){
                 [UtiltiyManager showAlertWithMessage:@"Server unresponsive. Please try again."];
            }else{
                self.dataSource = object;
                 [self loadData];
            }
        }];
    }else {
        self.dataSource = [StorageManager getPostList];
        [self loadData];
    }

    
}
- (void)setUIWithDataSource:(Menu *)object {
    if ([UtiltiyManager isReachableToInternet]) {
        self.currpageDetails = 1;
        self.menu1 = object;
        [WebServiceManager fetchMenuDetails:object.menuObjectId withPage:self.currpageDetails onCompletion:^(id object, NSError *error) {
            if (error) {
                 [UtiltiyManager showAlertWithMessage:@"Server unresponsive. Please try again."];
            }else {
                self.prevpageDetails = self.currpageDetails;
                self.currpageDetails= self.currpageDetails +  1;
                self.dataSource = object;
                [self.tblPost reloadData];
               
                [StorageManager savePostList:self.dataSource];
                [self loadData];
            }
        }];
    }else {
        self.dataSource = [StorageManager getPostList];
        [self loadData];
    }

}
- (void)loadData {
    [self.tblPost reloadData];
    self.tblPost.hidden = NO;
    if (self.dataSource.count > 0) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.tblPost scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}
- (void)setEventOnCompletion:(EventCompletionHandler )handler {
    eventCompletionHandler = handler;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.isPostPage = 1;
    [self setUpNib];
    [self setSliderSetUp];
    self.tblPost.hidden = YES;
    [self loadRecentPost];
    refreshControl = [[UIRefreshControl alloc] init];
   [refreshControl addTarget:self action:@selector(sortArray) forControlEvents:UIControlEventValueChanged];
   [self.tblPost addSubview:refreshControl];
     [self.tblPost setContentOffset:CGPointMake(0, 0) animated:NO];
   [self setGoogleAdevrtisement];
    appDelegate.isShowingNavLogo = NO;
    if (appDelegate.isShowingNavLogo) {
        UIImage *image = [UIImage imageNamed:kLogoImageName];
        UIImageView *imgvTitle=[[UIImageView alloc] initWithImage:image];
        [imgvTitle setFrame:CGRectMake(0, 0, 100, 20)];
        imgvTitle.contentMode = UIViewContentModeScaleAspectFit;
        self.navigationItem.titleView = imgvTitle ;
    }
    
}
- (void)loadRecentPost{
    if ([UtiltiyManager isReachableToInternet]) {
        self.currpage = 1;
        [WebServiceManager fetchRecentPostPagewiseOnCompletion:self.currpage onCompletion:^(id object, NSError *error) {
            if(error){
                [UtiltiyManager showAlertWithMessage:@"Server unresponsive. Please try again."];
            }else{
                
                    self.prevpage = self.currpage;
                    self.currpage= self.currpage +  1;
                    self.dataSource = object;
                    [StorageManager savePostList:self.dataSource];
                    [self loadData];
                

            }
        }];
        

    }else {
        self.dataSource = [StorageManager getPostList];
        [self loadData];
    }

}
- (void)sortArray{
   
    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
    if ([UtiltiyManager isReachableToInternet]) {
        [WebServiceManager fetchRecentPostOnCompletion:^(id object, NSError *error){
            if (error) {
                 [UtiltiyManager showAlertWithMessage:@"Server unresponsive. Please try again."];
            }else {
                self.dataSource = object;
                [StorageManager savePostList:self.dataSource];
                [self loadData];
            }
        }];
    }else {
        self.dataSource = [StorageManager getPostList];
        [self loadData];
        
    }
    [refreshControl endRefreshing];
    });
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    float offs = (scrollView.contentOffset.y+scrollView.bounds.size.height);
    float val = (scrollView.contentSize.height);
   
    if ((int)offs < (int)val)
    {
        NSLog(@"current=%dprev=%d",self.currpage,self.prevpage);
        if(self.posttype == selectMenuDetails){
            if(self.currpageDetails != self.prevpageDetails){
                self.prevpageDetails = self.currpageDetails;
                [WebServiceManager fetchMenuDetails:self.menu1.menuObjectId withPage:self.currpageDetails onCompletion:^(id object, NSError *error) {
                    if(error){
                        [UtiltiyManager showAlertWithMessage:@"Server unresponsive. Please try again."];
                    }else{
                        NSMutableArray *newPost = [[NSMutableArray alloc]init];
                        newPost = object;
                        [newPost enumerateObjectsUsingBlock:^(Post *post, NSUInteger idx, BOOL *stop) {
                            [self.dataSource addObject:post];
                        }];
                        self.currpageDetails= self.currpageDetails +  1;
                        [StorageManager savePostList:newPost];
                        [self.tblPost reloadData];
                        // [self loadData];
                        
                    }
                }];
            }

        }else{
            if(self.currpage != self.prevpage){
                self.prevpage = self.currpage;
                [WebServiceManager fetchRecentPostPagewiseOnCompletion:self.currpage onCompletion:^(id object, NSError *error) {
                    if(error){
                        [UtiltiyManager showAlertWithMessage:@"Server unresponsive. Please try again."];
                    }else{
                        NSMutableArray *newPost = [[NSMutableArray alloc]init];
                        newPost = object;
                        [newPost enumerateObjectsUsingBlock:^(Post *post, NSUInteger idx, BOOL *stop) {
                            [self.dataSource addObject:post];
                        }];
                        self.currpage= self.currpage +  1;
                        [StorageManager savePostList:newPost];
                        [self.tblPost reloadData];
                        // [self loadData];
                        
                    }
                }];
            }

        }
        
            
        
        
    }
}
- (void)setUpNib{
    static NSString *CellIdentifier1 = @"CustomPostListCell";
    static NSString *CellIdentifier2 = @"CustomPostListCell2";
    static NSString *CellIdentifier3 = @"CustomPostListCell3";
    static NSString *CellIdentifier4 = @"CustomPostListCell4";
    
    if (!iPad ){
        UINib *nib = [UINib nibWithNibName:@"CustomTemplate1" bundle:nil];
        [self.tblPost registerNib:nib forCellReuseIdentifier:CellIdentifier1];
        
        nib = [UINib nibWithNibName:@"CustomTemplate2" bundle:nil];
        [self.tblPost registerNib:nib forCellReuseIdentifier:CellIdentifier2];
        
        nib = [UINib nibWithNibName:@"CustomTemplate3" bundle:nil];
        [self.tblPost registerNib:nib forCellReuseIdentifier:CellIdentifier3];
        
        nib = [UINib nibWithNibName:@"CustomTemplate4" bundle:nil];
        [self.tblPost registerNib:nib forCellReuseIdentifier:CellIdentifier4];

        }
//    else{
//        UINib *nib = [UINib nibWithNibName:@"CustomTemplate1_Ipad" bundle:nil];
//        [self.tblPost registerNib:nib forCellReuseIdentifier:CellIdentifier1];
//        
//        nib = [UINib nibWithNibName:@"CustomTemplate2_Ipad" bundle:nil];
//        [self.tblPost registerNib:nib forCellReuseIdentifier:CellIdentifier2];
//        
//        nib = [UINib nibWithNibName:@"CustomTemplate3_Ipad" bundle:nil];
//        [self.tblPost registerNib:nib forCellReuseIdentifier:CellIdentifier3];
//        
//        nib = [UINib nibWithNibName:@"CustomTemplate4_Ipad" bundle:nil];
//        [self.tblPost registerNib:nib forCellReuseIdentifier:CellIdentifier4];
//    }
}
#pragma mark Table View Data Source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return self.dataSource.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
        Post *post = [self.dataSource objectAtIndex:indexPath.row];
        if([post.postTemplate isEqualToString:@"3"]){
            return [CustomPostListCell3 getDynamicHeightOfCellOfPost:post];
        }else if ([post.postTemplate isEqualToString:@"2"]){
            return [CustomPostListCell2 getDynamicHeightOfCellOfPost:post];
        }else{
            if([post.postThumb isEqualToString:@""]){
                return [CustomPostListCell getDynamicHeightOfCellOfPost:post];
            }else{
                return [CustomPostListCell4 getDynamicHeightOfCellOfPost:post];
            }
            
        }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"CustomPostListCell";
    static NSString *cellIdentifier2 = @"CustomPostListCell2";
    static NSString *cellIdentifier3 = @"CustomPostListCell3";
    static NSString *cellIdentifier4 = @"CustomPostListCell4";
     Post *post = [self.dataSource objectAtIndex:indexPath.row];
    if([post.postTemplate isEqualToString:@"3"]){
        CustomPostListCell3 *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier3];
        if (!cell) {
            cell = [[CustomPostListCell3 alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier3];
        }
         [cell setUIWithDataSource:post];
         return cell;
    }else if ([post.postTemplate isEqualToString:@"2"]){
        CustomPostListCell2 *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier2];
        if (!cell) {
            cell = [[CustomPostListCell2 alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier2];
        }
         [cell setUIWithDataSource:post];
         return cell;
    }else{
         if([post.postThumb isEqualToString:@""]){
             CustomPostListCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
             if (!cell) {
                 cell = [[CustomPostListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
             }
             [cell setUIWithDataSource:post];
              return cell;
         }else{
             CustomPostListCell4 *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier4 forIndexPath:indexPath];
             if (!cell) {
                 cell = [[CustomPostListCell4 alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier4];
             }
             [cell setUIWithDataSourceImage:post forIndexPath:indexPath];
             return cell;
         }
        
       
    }
}
#pragma mark Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    appDelegate.isPostPage = 2;
     self.navigationItem.title = @"Back";
     Post *post = [self.dataSource objectAtIndex:indexPath.row];
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
    if([post.postWebEnable isEqualToString:@"1"]){
        NSURL *strUrl = [NSURL URLWithString:post.postUrl];
        SVWebViewController *objSettingsVC  =  [[SVWebViewController alloc] initWithURL:strUrl];
        [self.navigationController pushViewController:objSettingsVC animated:YES];
    }else{
    PostDetailsVC *postDetails = [story instantiateViewControllerWithIdentifier:@"PostDetailsVC"];
    postDetails.post = post;
    [self.navigationController pushViewController:postDetails animated:YES];
    }
}

#pragma mark memory warnings
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
#pragma mark slider set up
- (void)setSliderSetUp {
    self.navigationItem.title = @"Post";
   self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"nav_menu_icon.png"] style:UIBarButtonItemStylePlain target:self action:@selector(goToMenu:)];
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"settingBtn.png"] style:UIBarButtonItemStylePlain target:self action:@selector(settingsPressed:)];
	[self addPanGesture];
}
- (void)goToMenu:(id)sender {
  if (eventCompletionHandler) {
      eventCompletionHandler (nil,kDraggingEvent);
  }
}
-(void)settingsPressed:(id)sender{
    if(!appDelegate.isIpad){
       CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenHeight = screenRect.size.height;
        if(screenHeight > 480){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
        SettingsVC *objSettingsVC = [storyboard instantiateViewControllerWithIdentifier:@"SettingsVC"];
        [self.navigationController pushViewController:objSettingsVC animated:YES];
        }else{
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"StoryboardSmall" bundle:nil];
            SettingsVC *objSettingsVC = [storyboard instantiateViewControllerWithIdentifier:@"SettingsVC"];
            [self.navigationController pushViewController:objSettingsVC animated:YES];
        }
    }
    self.navigationItem.title = @"Back";
}
- (void)addPanGesture {
    UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    panGestureRecognizer.minimumNumberOfTouches = 1.0;
    self.view.userInteractionEnabled = YES;
    [self.navigationController.view addGestureRecognizer:panGestureRecognizer];
}
- (void)handlePan:(UIPanGestureRecognizer *)recognizer {
    CGPoint translation = [recognizer translationInView:recognizer.view];
    recognizer.view.center=CGPointMake(recognizer.view.center.x+translation.x, recognizer.view.center.y);
    [recognizer setTranslation:CGPointMake(0, 0) inView:recognizer.view];
    if (recognizer.state == UIGestureRecognizerStateEnded ) {
        CGRect rect = self.view.frame;
        if (recognizer.view.frame.origin.x > self.view.frame.size.width/2) {
            if(iPad){
                rect.origin.x = rect.size.width *0.39;
            }else{
                 rect.origin.x = rect.size.width *0.80;
            }
            
        }else {
            rect.origin.x = 0.0;
        }
        [UIView animateWithDuration:0.3 animations:^(void){
        	self.navigationController.view.frame = rect;
        }];
    }
}

- (void)panRecognized:(UIPanGestureRecognizer *)recognizer
{
    CGPoint vel = [recognizer velocityInView:self.view];
    if (vel.x > 0)
    {
        CGPoint translation = [recognizer translationInView:recognizer.view];
        recognizer.view.center=CGPointMake(recognizer.view.center.x+translation.x, recognizer.view.center.y);
        [recognizer setTranslation:CGPointMake(0, 0) inView:recognizer.view];
        if (recognizer.state == UIGestureRecognizerStateEnded ) {
            CGRect rect = self.view.frame;
            if (recognizer.view.frame.origin.x > self.view.frame.origin.x/2) {
                if(iPad){
                    rect.origin.x = rect.size.width *0.30;
                }else{
                    rect.origin.x = rect.size.width *0.80;
                }
                
            }else {
                rect.origin.x = 0.0;
            }
            [UIView animateWithDuration:0.3 animations:^(void){
                self.navigationController.view.frame = rect;
            }];
        }

    }
}
#pragma mark Google advertisement 
- (void)setGoogleAdevrtisement {
    self.adModel = [GoogleAdServiceManager getAdModelInfo];
    [self setAdvertisement];
}
- (void)setAdvertisement {
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenHeight = screenRect.size.height;
    if (self.adModel.adMobType == kSmallImageBanner) {
        UIButton *addButton = [UIButton buttonWithType:UIButtonTypeCustom];
        if(iPhone){
             addButton.frame =   CGRectMake(0, 64, 320, 50);
        }else{
             addButton.frame =   CGRectMake(0, 64, 768, 66);
        }
        UIImage *image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.adModel.adMobImageLink]]];
        [addButton setBackgroundImage:image forState:UIControlStateNormal];
        [addButton addTarget:self
                      action:@selector(adSmallBannerImageTapped:)
            forControlEvents:(UIControlEvents)UIControlEventTouchDown];
        [self.view addSubview:addButton];
        if(iPhone){
            CGRect screenRect = [[UIScreen mainScreen] bounds];
            CGFloat screenHeight = screenRect.size.height;
            if(screenHeight > 480){
                self.tblPost.frame = CGRectMake(0, 54, 320, 514);
            }else{
                self.tblPost.frame = CGRectMake(0, 54, 320, 426);
            }
        }else{
            self.tblPost.frame = CGRectMake(0, 66, 768, 958);
        }
        
        
        
    }else if (self.adModel.adMobType == kGoogleAdMob){
        
        GADBannerView *gad = [[GADBannerView alloc]init];
        if(iPhone){
            gad.frame = CGRectMake(0, 64, 320, 50);
        }else{
            gad.frame =   CGRectMake(0, 64, 768, 66);
        }
        [self.view addSubview:gad];
        gad.adUnitID = @"ca-app-pub-9343839896861016/7703717299";
        gad.rootViewController = self;
        
        GADRequest *request = [GADRequest request];
        // Enable test ads on simulators.
        request.testDevices = @[ GAD_SIMULATOR_ID ];
        [gad loadRequest:request];

        if(iPhone){
            
            CGRect screenRect = [[UIScreen mainScreen] bounds];
            CGFloat screenHeight = screenRect.size.height;
            if(screenHeight > 480){
            self.tblPost.frame = CGRectMake(0, 54, 320, 514);
            }else{
            self.tblPost.frame = CGRectMake(0, 54, 320, 426);
            }
        }else{
            self.tblPost.frame = CGRectMake(0, 66, 768, 958);
        }
        
    }
    else if (self.adModel.adMobType==kHTMLSnippet){
        UIWebView *myWebView;
         if(iPhone){
             myWebView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 64, 320, 50)];
         }else{
             myWebView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 64, 768, 66)];
         }
        myWebView.scalesPageToFit = YES;
        myWebView.delegate=self;
        NSString *html = self.adModel.adMobHtmlSnippet;
        NSString *javascript = self.adModel.adMobHTMLJSScript;
        html= [html stringByAppendingString:javascript];
        [myWebView loadHTMLString:html baseURL:nil];
        [self.view addSubview:myWebView];
        [self addTapGestureInView:myWebView];
        if(iPhone){
            CGRect screenRect = [[UIScreen mainScreen] bounds];
            CGFloat screenHeight = screenRect.size.height;
            if(screenHeight > 480){
                self.tblPost.frame = CGRectMake(0, 54, 320, 514);
            }else{
                self.tblPost.frame = CGRectMake(0, 54, 320, 426);
            }
        }else{
           self.tblPost.frame = CGRectMake(0, 66, 768, 958);
        }
        
    }else {
        if(iPhone){
            if(screenHeight > 480){
                 self.tblPost.frame = CGRectMake(0, 0, 320, 568);
            }else{
                self.tblPost.frame = CGRectMake(0, 0, 320, 480);
              
            }
       
        self.tblPost.contentInset = UIEdgeInsetsMake(8, 0, 0, 0);
            
        }else{
            self.tblPost.frame = CGRectMake(0, 0, 768, 1024);
           
        }
    }
}
- (void)addTapGestureInView :(UIWebView *)webView {
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:nil];
    tapGesture.numberOfTapsRequired = 1;
    webView.userInteractionEnabled = YES;
    tapGesture.delegate = self;
    [webView addGestureRecognizer:tapGesture];
}
-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
     if ( inType == UIWebViewNavigationTypeLinkClicked ) {
         NSString *myString = [[inRequest URL] absoluteString];
//         iPhoneWebView *ip = [[iPhoneWebView alloc] init];
//         [ip loadUrlInWebView:myString];
//         [self.navigationController pushViewController:ip animated:YES];
         SVWebViewController *web = [[SVWebViewController alloc]initWithAddress:myString];
         [self.navigationController pushViewController:web animated:YES];

     }
    return YES;
}
- (void)tapOnAd :(id)sender {
//    HTMLAd *htmlAd = [[HTMLAd alloc] init];
//    NSString *html = self.adModel.adMobHtmlSnippet;
//    NSString *javascript = self.adModel.adMobHTMLJSScript;
//    html= [html stringByAppendingString:javascript];
//    htmlAd.htmlSnippet = html;
//    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:htmlAd];
//    [self presentViewController:navigationController animated:YES completion:NULL];
//    SVWebViewController *web = [[SVWebViewController alloc]init];
//    [web loadHtml:html withJavascript:javascript];
//    [self.navigationController pushViewController:web animated:YES];
    /*if (!isShowingFullScreenAdd) {
        isShowingFullScreenAdd = YES;
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:htmlAd];
        [self presentViewController:navigationController animated:YES completion:NULL];
    }*/
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}
- (void)adSmallBannerImageTapped:(id)sender {
//    HTMLAd *htmlAd = [[HTMLAd alloc] init];
//    htmlAd.isSmallImageBanner = YES;
//    htmlAd.webViewImageLink = self.adModel.adMobWebViewLink;
    
    SVWebViewController *newLink = [[SVWebViewController alloc]initWithAddress:self.adModel.adMobWebViewLink];
    [self.navigationController pushViewController:newLink animated:YES];
//    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:htmlAd];
//    [self presentViewController:navigationController animated:YES completion:NULL];
}
- (void)adBannerImageTapped:(id)sender {
    HTMLAd *htmlAd = [[HTMLAd alloc] init];
    BOOL IS_IPHONE_5 = ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON );
    htmlAd.buttonImageLink = IS_IPHONE_5 ?  self.adModel.adMobFullImageIphone5Link : self.adModel.adMobFullImageLink;
    htmlAd.webViewImageLink = self.adModel.adMobWebViewLink;
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:htmlAd];
    [self presentViewController:navigationController animated:YES completion:NULL];
}
- (void)adBannerTapped:(id)sender {
    HTMLAd *htmlAd = [[HTMLAd alloc] init];
    htmlAd.buttonImageLink = self.adModel.adMobImageLink;
    htmlAd.webViewImageLink = self.adModel.adMobWebViewLink;
    /*if (!isShowingFullScreenAdd) {
        isShowingFullScreenAdd = YES;
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:htmlAd];
        [self presentViewController:navigationController animated:YES completion:NULL];
    }*/
}

@end
