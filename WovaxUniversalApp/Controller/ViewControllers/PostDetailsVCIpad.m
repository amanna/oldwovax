//
//  PostDetailsVCIpad.m
//  WovaxUniversalApp
//
//  Created by Amrita on 08/09/14.
//  Copyright (c) 2014 Susim Samanta. All rights reserved.
//

#import "PostDetailsVCIpad.h"
#import "MWPhotoBrowser.h"
#import "SVWebViewController.h"
#import "MapVC.h"
#import "SettingsVCIPad.h"
@interface PostDetailsVCIpad ()

@property (nonatomic, strong) UIPopoverController *barButtonItemPopover;

@property (nonatomic, strong) UIPopoverController *detailViewPopover;
@property (nonatomic, strong) id lastTappedButton;

@property (nonatomic, strong) UIPopoverController *masterPopoverController;

@property (nonatomic, weak) IBOutlet UILabel *detailLabel;
@property (weak, nonatomic) IBOutlet UIWebView *myWebView;
@property(nonatomic)NSMutableArray *photos;
@property(nonatomic)NSMutableArray *thumbs;

@end

@implementation PostDetailsVCIpad
#pragma mark - Split view support

- (void)splitViewController: (UISplitViewController*)svc willHideViewController:(UIViewController *)aViewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController*)popoverController
{
    barButtonItem.title = NSLocalizedString(@"Master", @"Master");
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
    self.masterPopoverController = popoverController;
}


// Called when the view is shown again in the split view, invalidating the button and popover controller.
- (void)splitViewController: (UISplitViewController*)svc willShowViewController:(UIViewController *)aViewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    // Called when the view is shown again in the split view, invalidating the button and popover controller.
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
    self.masterPopoverController = nil;
}
- (BOOL)splitViewController:(UISplitViewController *)svc shouldHideViewController:(UIViewController *)vc inOrientation:(UIInterfaceOrientation)orientation{
    return NO;
}

// Called when the hidden view controller is about to be displayed in a popover.
- (void)splitViewController:(UISplitViewController*)svc popoverController:(UIPopoverController*)pc willPresentViewController:(UIViewController *)aViewController
{
	// Check whether the popover presented from the "Tap" UIBarButtonItem is visible.
	if ([self.barButtonItemPopover isPopoverVisible])
    {
		// Dismiss the popover.
        [self.barButtonItemPopover dismissPopoverAnimated:YES];
    }
}


#pragma mark - Rotation support
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
	// If the detail popover is presented, dismiss it.
	if (self.detailViewPopover != nil)
    {
		[self.detailViewPopover dismissPopoverAnimated:YES];
	}
}


- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
	// If the last button tapped is not nil, present the popover from that button.
	if (self.lastTappedButton != nil)
    {
		[self showPopover:self.lastTappedButton];
	}
}
- (IBAction)showPopover:(id)sender
{
	// Set the sender to a UIButton.
	UIButton *tappedButton = (UIButton *)sender;
	
	// Present the popover from the button that was tapped in the detail view.
	[self.detailViewPopover presentPopoverFromRect:tappedButton.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    
	// Set the last button tapped to the current button that was tapped.
	 self.lastTappedButton = sender;
}
#pragma mark - Managing the detail item

// When setting the detail item, update the view and dismiss the popover controller if it's showing
- (void)setDetailItem:(id)newDetailItem
{
    if (self.detailItem != newDetailItem)
    {
        _detailItem = newDetailItem;
        [self configureView];
    }
    
    if (self.masterPopoverController != nil)
    {
        [self.masterPopoverController dismissPopoverAnimated:YES];
    }
}


- (void)configureView
{
    // Update the user interface for the detail item.
    self.detailLabel.text = self.detailItem;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.barButtonItemPopover dismissPopoverAnimated:YES];
    UIBarButtonItem *actionBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(actionBtnClicked)];
    
    if([self.post.postMapEnable isEqualToString:@"1"]){
        UIBarButtonItem *mapButton = [[UIBarButtonItem alloc]
                                      initWithImage:[UIImage imageNamed:@"723-location-arrow.png"]
                                      style:UIBarButtonItemStylePlain
                                      target:self action:@selector(goToMapView)];
        
        self.navigationItem.rightBarButtonItems = [[NSArray alloc] initWithObjects:actionBtn, mapButton, nil];
        
    }else{
        self.navigationItem.rightBarButtonItem = actionBtn;
    }
    

    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"settingBtn.png"] style:UIBarButtonItemStylePlain target:self action:@selector(settingsPressed:)];

    
    // Do any additional setup after loading the view.
}
-(void)settingsPressed:(id)sender{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"StoryboardIPad" bundle:nil];
        SettingsVCIPad *objSettingsVC = [storyboard instantiateViewControllerWithIdentifier:@"SettingsVCIPad"];
        [self.navigationController pushViewController:objSettingsVC animated:YES];
        //self.navigationItem.title = @"Back";
}

- (void)goToMapView{
    MapVC *mapVC;
    if(iPhone){
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenHeight = screenRect.size.height;
        if(screenHeight > 480){
            UIStoryboard *story = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
            mapVC = [story instantiateViewControllerWithIdentifier:@"MapVC"];
            mapVC.post = self.post;
        }else{
            UIStoryboard *story = [UIStoryboard storyboardWithName:@"StoryboardSmall" bundle:nil];
            mapVC = [story instantiateViewControllerWithIdentifier:@"MapVC"];
            mapVC.post = self.post;
        }
        
    }else{
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"StoryboardIPad" bundle:nil];
        mapVC = [story instantiateViewControllerWithIdentifier:@"MapVC"];
        mapVC.post = self.post;
    }
    [self.navigationController pushViewController:mapVC animated:YES];
}


- (void)actionBtnClicked{
    NSArray *objectsToShare;
    NSString *text = self.post.postTitle;
    NSURL *url = [NSURL URLWithString:self.post.postUrl];
    if(![self.post.postType isEqualToString:@"page"]){
        NSURL *imageUrl = [NSURL URLWithString:self.post.postThumb];
        UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:imageUrl]];
        if(image==nil){
            objectsToShare = @[text, url];
        }else{
            objectsToShare = @[text, url,image];
        }
        
    }else{
        objectsToShare = @[text, url];
    }
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypePostToFacebook
                                   ];
    
    activityVC.excludedActivityTypes = excludeActivities;
    //[self presentViewController:activityVC animated:YES completion:nil];
    
    
    UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:activityVC];
    [popup presentPopoverFromRect:CGRectMake(300,-30, 100, 100) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

-(void)loadPostDetails {
    NSString *path;
    if([self.post.postType isEqualToString:@"page"]){
        if([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
            path = [[NSBundle mainBundle] pathForResource:@"detailspageIpad" ofType:@"html"];
        }else{
            path = [[NSBundle mainBundle] pathForResource:@"detailspage" ofType:@"html"];
        }
    }else{
        if([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
            path = [[NSBundle mainBundle] pathForResource:@"detailspostIpad" ofType:@"html"];
        }else{
            path = [[NSBundle mainBundle] pathForResource:@"detailspost" ofType:@"html"];
        }
        
    }
    
    NSString *html = [[NSString alloc] initWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    html = [html stringByReplacingOccurrencesOfString:@"$baseurl$" withString:kBaseURL];
    html = [html stringByReplacingOccurrencesOfString:@"$title$" withString:self.post.postTitle];
    html = [html stringByReplacingOccurrencesOfString:@"$date$" withString:self.post.postDate];
    html = [html stringByReplacingOccurrencesOfString:@"$name$" withString:self.post.postBy];
    html = [html stringByReplacingOccurrencesOfString:@"$image$" withString:self.post.postThumb?self.post.postThumb : @"" ];
    html = [html stringByReplacingOccurrencesOfString:@"$content$" withString:self.post.postContent];
    html = [html stringByReplacingOccurrencesOfString:@"$post_id$" withString:self.post.postId];
    
    [self.myWebView loadHTMLString:html baseURL:nil];
    
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString *padding = @"document.body.style.margin='0';document.body.style.padding = '0'";
    [webView stringByEvaluatingJavaScriptFromString:padding];
    if([self.post.postGalleryEnable isEqualToString:@"1"]){
        NSString *gallery = @"function getJson() { $('#get_post_gallery').load('$baseurl$/?wovaxmenu=get_post_gallery&id=$postid$'); } getJson();";
        gallery = [gallery stringByReplacingOccurrencesOfString:@"$postid$" withString:self.post.postId];
        gallery = [gallery stringByReplacingOccurrencesOfString:@"$baseurl$" withString:kBaseURL];
        [self.myWebView stringByEvaluatingJavaScriptFromString:gallery];
        
    }
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)aRequest navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        NSString *myString = [[aRequest URL] absoluteString];
        NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"http://photo"] invertedSet];
        if([myString rangeOfCharacterFromSet:set].location != NSNotFound){
            NSArray *arys = [myString componentsSeparatedByString:@";"];
            if([arys count] == 1)
            {
                NSLog(@"WebView");
                NSString *lastThreeChars = [myString substringFromIndex:myString.length-3];
                if ([lastThreeChars isEqualToString:@"jpg"] || [lastThreeChars isEqualToString:@"png"]) {
                    //                    EGOPhotoViewController *photoController = [[EGOPhotoViewController alloc] initWithImageURL:[NSURL URLWithString:myString]];
                    //                    [[AppDelegate instance] setLandscapeMode:YES];
                    //                    [self.navigationController pushViewController:photoController animated:YES];
                }else {
                    
                    [self gotONewWebView:myString];
                }
                return NO;
            }
            else
            {
                if([self.post.postGalleryEnable isEqualToString:@"1"]){
                    
                    // push new webview with image ...
                    [self goToBrowsePhoto];
                }
                return YES;
            }
            return YES;
        }
        return YES;
        // check if user clicked on your image...clickedOnImageWithUrl is made up
    }
    return YES;
    
}
- (void)gotONewWebView:(NSString*)myString{
    NSString *str = [NSString stringWithFormat:@"%@",myString];
    SVWebViewController *objSettingsVC = [[SVWebViewController alloc]initWithAddress:str];
    objSettingsVC.modalPresentationStyle = UIModalPresentationPageSheet;
    [self.navigationController pushViewController:objSettingsVC animated:YES];
}
- (void)goToBrowsePhoto{
    // Create array of MWPhoto objects
    self.photos = [NSMutableArray array];
    self.thumbs = [NSMutableArray array];
    // Photos
    __block MWPhoto *photo;
    
    BOOL displayNavArrows = NO;
    BOOL startOnGrid = NO;
    
    
    for (NSDictionary *dict in self.post.postAttachment) {
        NSDictionary *dictImage = [dict valueForKey:@"images"];
        NSDictionary *fulldict = [dictImage valueForKey:@"full"];
        NSString *strImageUrl = [NSString stringWithFormat:@"%@",[fulldict valueForKey:@"url"]];
        photo = [MWPhoto photoWithURL:[NSURL URLWithString:strImageUrl]];
        [self.photos addObject:photo];
    }
    // Thumbs
    for (NSDictionary *dict in self.post.postAttachment) {
        NSDictionary *dictImage = [dict valueForKey:@"images"];
        NSDictionary *fulldict = [dictImage valueForKey:@"thumbnail"];
        NSString *strImageUrl = [NSString stringWithFormat:@"%@",[fulldict valueForKey:@"url"]];
        photo = [MWPhoto photoWithURL:[NSURL URLWithString:strImageUrl]];
        [self.thumbs addObject:photo];
    }
    
    startOnGrid = YES;
    displayNavArrows = YES;
    
    
    // Create browser (must be done each time photo browser is
    // displayed. Photo browser objects cannot be re-used)
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    
    // Set options
    browser.displayActionButton = YES; // Show action button to allow sharing, copying, etc (defaults to YES)
    browser.displayNavArrows = NO; // Whether to display left and right nav arrows on toolbar (defaults to NO)
    browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
    browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
    browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
    browser.enableGrid = YES; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
    browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
    //browser.wantsFullScreenLayout = YES; // iOS 5 & 6 only: Decide if you want the photo browser full screen, i.e. whether the status bar is affected (defaults to YES)
    
    // Optionally set the current visible photo before displaying
    [browser setCurrentPhotoIndex:1];
    // Manipulate
    [self.navigationController pushViewController:browser animated:YES];
    [browser showNextPhotoAnimated:YES];
    [browser showPreviousPhotoAnimated:YES];
    [browser setCurrentPhotoIndex:0];
    
    
}
#pragma mark - MWPhotoBrowserDelegate
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return self.photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < self.photos.count)
        return [self.photos objectAtIndex:index];
    return nil;
}
- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser thumbPhotoAtIndex:(NSUInteger)index {
    if (index < _thumbs.count)
        return [_thumbs objectAtIndex:index];
    return nil;
}
- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser didDisplayPhotoAtIndex:(NSUInteger)index {
    NSLog(@"Did start viewing photo at index %lu", (unsigned long)index);
}


//- (NSString *)photoBrowser:(MWPhotoBrowser *)photoBrowser titleForPhotoAtIndex:(NSUInteger)index {
//    return [NSString stringWithFormat:@"Photo %lu", (unsigned long)index+1];
//}


- (void)photoBrowserDidFinishModalPresentation:(MWPhotoBrowser *)photoBrowser {
    // If we subscribe to this method we must dismiss the view controller ourselves
    NSLog(@"Did finish modal presentation");
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
