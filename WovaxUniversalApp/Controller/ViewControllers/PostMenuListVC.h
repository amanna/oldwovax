//
//  PostMenuListTableViewController.h
//  WovaxUniversalApp
//
//  Created by Amrita on 08/09/14.
//  Copyright (c) 2014 Susim Samanta. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PostDetailsVCIpad;

@interface PostMenuListVC : UITableViewController
@property (strong, nonatomic) IBOutlet PostDetailsVCIpad *detailViewController;
@end
