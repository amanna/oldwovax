//
//  SSMenuVC.h
//  SSMenuSlider
//
//  Created by Susim on 11/6/13.
//  Copyright (c) 2013 Susim. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Menu.h"

enum EventType {
    kActionEvent = 0,
    kDraggingEvent = 1
    };
typedef void (^EventCompletionHandler)(id object, NSUInteger eventType);
@interface SSMenuVC : UIViewController {
    EventCompletionHandler eventCompletionHandler;
}
@property(retain)  NSIndexPath* lastIndexPath;
@property(nonatomic)  NSInteger lastIndexRow;
- (void)setUIWithDataSource:(id)object;
- (void)setEventOnCompletion:(EventCompletionHandler)handler ;
@end
