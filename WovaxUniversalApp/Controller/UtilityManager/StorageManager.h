//
//  StorageManager.h
//  WovaxUniversalApp
//
//  Created by Susim on 6/16/14.
//  Copyright (c) 2014 Susim Samanta. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StorageManager : NSObject
+ (NSString *)getDocsDirectory ;
+ (void)saveMenuList:(NSArray *)menuList;
+ (NSArray *)getMenuList;
+ (void)savePostList:(NSArray *)postList;
+ (NSMutableArray *)getPostList;
@end
