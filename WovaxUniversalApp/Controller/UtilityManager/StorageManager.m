//
//  StorageManager.m
//  WovaxUniversalApp
//
//  Created by Susim on 6/16/14.
//  Copyright (c) 2014 Susim Samanta. All rights reserved.
//

#import "StorageManager.h"

@implementation StorageManager
+ (NSString *)getDocsDirectory {
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return  dirPaths[0];
}
+ (void)saveMenuList:(NSArray *)menuList {
    NSString *dataFilePath = [[NSString alloc] initWithString:[[self getDocsDirectory] stringByAppendingPathComponent:@"menu.plist"]];
    if ( [NSKeyedArchiver archiveRootObject:menuList toFile:dataFilePath]) {
        NSLog(@"Saved");
    }
}
+ (NSArray *)getMenuList {
    NSString *dataFilePath = [[NSString alloc] initWithString:[[self getDocsDirectory] stringByAppendingPathComponent:@"menu.plist"]];
    if ([[NSFileManager defaultManager] fileExistsAtPath:dataFilePath]) {
        NSArray *menuList = [NSKeyedUnarchiver unarchiveObjectWithFile:dataFilePath];
        return menuList;
    }
    return nil;
}
+ (void)savePostList:(NSArray *)postList {
    NSString *dataFilePath = [[NSString alloc] initWithString:[[self getDocsDirectory] stringByAppendingPathComponent:@"post.plist"]];
    if ( [NSKeyedArchiver archiveRootObject:postList toFile:dataFilePath]) {
        NSLog(@"Saved");
    }
}
+ (NSMutableArray *)getPostList {
    NSString *dataFilePath = [[NSString alloc] initWithString:[[self getDocsDirectory] stringByAppendingPathComponent:@"post.plist"]];
    if ([[NSFileManager defaultManager] fileExistsAtPath:dataFilePath]) {
        NSMutableArray *postList= [NSKeyedUnarchiver unarchiveObjectWithFile:dataFilePath];
        return postList;
    }
    return nil;
}
@end
