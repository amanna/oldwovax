//
//  main.m
//  WovaxUniversalApp
//
//  Created by Susim Samanta on 22/09/13.
//  Copyright (c) 2013 Susim Samanta. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
